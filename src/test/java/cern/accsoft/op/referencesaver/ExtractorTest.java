package cern.accsoft.op.referencesaver;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

import org.apache.spark.sql.SparkSession;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.DependsOn;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import cern.accsoft.op.referencesaver.nxcals.Extractor;
import cern.nxcals.api.config.SparkContext;
import lombok.extern.slf4j.Slf4j;

/**
*/

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = SparkContext.class)
@SpringBootApplication
@Slf4j
@DependsOn("kerberos")
public class ExtractorTest {

    @Autowired
	private SparkSession fSparkSession;

    private Extractor fExtractorService;
	
    public ExtractorTest()
    {
    	fExtractorService = new Extractor(fSparkSession);
    }
    
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		
		
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	public void testEntitySearch() {
		
		assertNotNull(fExtractorService, "Extractor service cannot be null");
		
		
	}

}
