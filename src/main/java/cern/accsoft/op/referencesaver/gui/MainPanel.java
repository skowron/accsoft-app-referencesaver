/**
 * Copyright (c) 2022 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.accsoft.op.referencesaver.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.CompletableFuture;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.DefaultHighlighter.DefaultHighlightPainter;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Element;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cern.accsoft.commons.ccs.AcceleratorInfo;
import cern.accsoft.commons.lsacontextmapping.LsaContextMapping;
import cern.accsoft.op.referencesaver.Config;
import cern.accsoft.op.referencesaver.ReferenceSaverGuiApp;
import cern.accsoft.op.referencesaver.co.LsaUtils;
import cern.accsoft.op.referencesaver.nxcals.Reference;
import cern.accsoft.op.referencesaver.nxcals.TrimHistory;
import cern.japc.value.MapParameterValue;
import cern.lsa.domain.settings.Setting;
import cern.lsa.domain.settings.TrimHeader;

public class MainPanel extends JPanel implements ActionListener, WindowListener, TreeSelectionListener{
    private static final long serialVersionUID = 5212340232433749221L;
    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());
    
    protected   JTabbedPane fTabs;
    /**
     * List of TGM users
     */
    private       JTextPane fUsersTextPane;
    
    /**
     * TGM User (keys) <--> lsa cycle name
     */
    TreeMap<String, String> fCycleMapping = null;
    /**
     * Because Map does not allow to get by index, fTgmUser keeps fCycleMapping ketSet as an array
     */
    String[]    fTgmUsers = null;  
    String fSelectedTgmUser = "";

    
    private JTable                 fReferencesTable;
    private DefaultTableModel fReferencesTableModel;
    
    enum ComponentIDs
    {
    	SaveButton, RetrieveButton, AppendLsaCycleName;
    	
    }
    
    private LinkedList<Long> fLastPulseCycleTimestamps = new LinkedList<Long>();
    
    JLabel fLastPulseLabel =  new JLabel("Last Pulse: xxx  Dest: xxx");
    JLabel  fNewRefLabel = new JLabel("new reference name: ");
    
    JTextField  fNewReferenceName;
    
    List<ReferenceViewPanel> fOpenReferences = new LinkedList<ReferenceViewPanel>();
    
    
    protected SimpleDateFormat fTimeFmt = new SimpleDateFormat("yyyy.MM.dd H:m:s");
    
    //_____________________________________________________________________
    public MainPanel() 
    {
        /**
         * Put your own code here The following sample is there only to illustrate the use of the
         * AscBeanUtilities.getDefaultAscBean(parameterName) method.
         */
        
        super(new BorderLayout());
        setSize(800, 1000);
        setMinimumSize(new Dimension(600,800));
        
        fTabs = new JTabbedPane();
        
        JComponent page1 = createMainTab();
        fTabs.addTab("Main",null,page1,"Reference Manager");

        add(fTabs, BorderLayout.CENTER);
        
    }
    //_____________________________________________________________________
    
    public void selectUser(CaretEvent e) {
    	fUsersTextPane.getHighlighter().removeAllHighlights();
    	
    	Element rootel = fUsersTextPane.getDocument().getDefaultRootElement();
    	int lineno = rootel.getElementIndex(e.getDot());
    	int from = rootel.getElement(lineno).getStartOffset();
    	int to = rootel.getElement(lineno).getEndOffset();
    	System.out.println("Current line is "+lineno);
    		
        try {
        	DefaultHighlightPainter hlp = new DefaultHighlightPainter(Color.DARK_GRAY);
        	fUsersTextPane.getHighlighter().addHighlight(from, to, hlp );           
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        fUsersTextPane.repaint();
        
        
    	if (fTgmUsers == null) return;
    	
    	if (lineno >= fTgmUsers.length ) 
    	{
    		LOGGER.error("Computed line is out of User Map range");
    		return;
    	}
    	
    	fSelectedTgmUser = fTgmUsers[lineno];
    	
    	
    	fReferencesTableModel.setRowCount(0);
    	fReferencesTable.setBackground(Color.red);
    	revalidate();
    	
    	CompletableFuture.supplyAsync(() -> updateListOfReferences());
    	 
    	System.out.println("Selected TGM user :" + fSelectedTgmUser);

    }
  //___________________________________________________________________

  public String getSelectedTgmUser()
  {
	  return fSelectedTgmUser;
  }
    //_____________________________________________________________________

  public Integer updateListOfReferences()
  {
	   //fReferencesTable.removeAll();
	   fReferencesTableModel.setRowCount(0);
	   
	   String lsaCycleName = fCycleMapping.get(fSelectedTgmUser);
	   
	   
	   Set<Setting> currentsettings = LsaUtils.findTrimValues(Config.getReferenceNameParam(), lsaCycleName, null);
	   
	   if (currentsettings.isEmpty())
	   {
		   String[] noTrims =  new String[] {"no trims","-","-"};
		   fReferencesTableModel.addRow( noTrims );
	   }
	   
	   Setting currentsetting  = null;
	   
	   for (Setting setting: currentsettings)
	   {
		   if (currentsetting == null && setting != null)
		   {
			  currentsetting = setting;   
		   }
		   
		   String refName = setting.getScalarValue().getString();
		   String datestr = fTimeFmt.format(setting.getCreationDate());
		   String[] lastTrim =  new String[] {refName,datestr,"-"};
		   fReferencesTableModel.addRow( lastTrim );
	   }
	   
	   
	   if (currentsetting == null)
	   {
		   fReferencesTable.setBackground(Color.green);
		   revalidate();
		   return 0;
	   }
	   
	   
	   List<TrimHeader> trims = LsaUtils.findTrims(Config.getReferenceNameParam(), lsaCycleName);
	   Collections.reverse(trims);
	   
	   for (TrimHeader trim: trims)
	   {
		   if (trim.getCreatedDate().equals(currentsetting.getCreationDate()) )
		   {  // this is the current seetting that we already have
			   fReferencesTableModel.setValueAt(trim.getDescription(), 0, 2);
			   continue;
		   }
		   
		   String refName = "<retriving from nxcals ...>";
		   String datestr =  fTimeFmt.format(trim.getCreatedDate());
		   
		   String comment = trim.getDescription();
		   String[] noTrims =  new String[] {refName,datestr,comment};
		   fReferencesTableModel.addRow( noTrims );
		  
	   }
	  
	   fReferencesTable.setBackground(Color.yellow);
	   repaint();
	   revalidate();
	   
	   
	   TrimHistory trimSrv = ReferenceSaverGuiApp.getInstance().getTrimHistoryService();
	   
	   Map<Date, MapParameterValue> trimvalues = trimSrv.getTrims(Config.getReferenceNameParam(), lsaCycleName);
	   
	   //Vector<Vector> tabledata = fReferencesTableModel.getDataVector();
	   
	   for (Date t: trimvalues.keySet())
	   {
		   if (t.equals(currentsetting.getCreationDate()))
		   {
			   continue;
		   }
		   
		   String datestr =  fTimeFmt.format(t);
		   int found = -1;
		   int lastBigger = 0;
		   for (int i=1;i<fReferencesTableModel.getRowCount();i++)
		    {
		       String rdstr =  (String) fReferencesTableModel.getValueAt(i, 1);
		       if (rdstr.equals(datestr))
		       {
		    	   found = i;
		    	   break;
		       }
		       Date rd = null;
		       try {
				 rd = fTimeFmt.parse(rdstr);
			   } 
		       catch (ParseException e) 
		        {
		    	   continue;
			    }
		       if (rd.after(t))
		       {
		    	   lastBigger = i;
		       }
		    }
		   
		   String refName = trimvalues.get(t).getString("value");
		   String comment = " - ";
		   if (refName == null)
		   {
			   LOGGER.info("No value in the trim");
		   }
		   if (found > 0)
		   {
			   fReferencesTableModel.setValueAt(refName, found, 0);
		   }
		   else
		   {
			   comment = "Not found in trim history";
		       String[] atrim =  new String[] {refName,datestr,comment};
		       
		       if (lastBigger+1 >= fReferencesTableModel.getRowCount())
		       {
		    	 fReferencesTableModel.insertRow(lastBigger+1, atrim);  
		       }
		       else
		       {
		         fReferencesTableModel.addRow(atrim);
		       }
		   }
	   }
	   
	   fReferencesTable.setBackground(Color.green);
	   revalidate();
	   //tmodel.addRow()
	   
	   return fReferencesTableModel.getRowCount();

  }
  //_____________________________________________________________________

  private void createUsersList()
  {
      fUsersTextPane = new JTextPane();
      fUsersTextPane.setHighlighter(new DefaultHighlighter());
      fUsersTextPane.addCaretListener(new CaretListener() {
          @Override
          public void caretUpdate(CaretEvent e) {
        	  Runnable method = () -> {selectUser(e);};
        	  SwingUtilities.invokeLater( method );
              
          }
      });
      
      fUsersTextPane.setSize(300, 500);
      fUsersTextPane.setMaximumSize(new Dimension(4300,3000));
      fUsersTextPane.setMinimumSize(new Dimension(400,500));
      fUsersTextPane.setDoubleBuffered(true);

      fUsersTextPane.setBackground(Color.black);
      fUsersTextPane.setForeground(Color.white);
      Font font = new Font(Font.MONOSPACED, Font.PLAIN, 13);
      fUsersTextPane.setFont(font);
      StyledDocument stydoc = fUsersTextPane.getStyledDocument();
      Style tgmUserStyle = stydoc.addStyle("tgmUserStyle", null);
      Style lsaUserStyle = stydoc.addStyle("lsaUserStyle", null);
      
      StyleConstants.setBackground(tgmUserStyle, Color.black);
      StyleConstants.setForeground(tgmUserStyle, Color.green);

      StyleConstants.setBackground(lsaUserStyle, Color.black);
      StyleConstants.setForeground(lsaUserStyle, Color.white);
      
      
      LsaContextMapping lsaContextMapping = LsaContextMapping.getInstance(AcceleratorInfo.getCernAcceleratorForName("PSB"));
      
      Set<Long> tss = lsaContextMapping.getLocalHistoryTimeStamps();
      if (tss.isEmpty())
      {
    	  return;
      }
      
      System.out.println("Context mapping timestamps: "+ tss);

      Long ts = tss.iterator().next();
      
      fCycleMapping = new TreeMap<String, String>();
      fCycleMapping.putAll(lsaContextMapping.getLocalHistoryEntryForTimeStamp(ts));
      fTgmUsers = fCycleMapping.keySet().toArray(new String[fCycleMapping.keySet().size()]); 
      
      StyledDocument doc = new DefaultStyledDocument();
      

      try
      {
      for (Entry<String, String> e : fCycleMapping.entrySet())
      {	
    	  if(e.getKey().contains("multiple"))
    	  {
    		  continue;
    	  }
    	  
    	  doc.insertString(doc.getLength(),  e.getKey(), tgmUserStyle);
    	  doc.insertString(doc.getLength(),  "\t "+e.getValue()+"\n", lsaUserStyle);
    	  System.out.println("createUsersList: adding "+e.getKey()+" "+e.getValue());
    	  
      }
      }
      catch (BadLocationException e1) 
      {
          LOGGER.error("Sth wrong with location",e1);
      }
      
      fUsersTextPane.setDocument(doc);
  }
  //___________________________________________________
  private JComponent createMainTab()
  {
	  JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEADING));

      createUsersList();
      panel.add(fUsersTextPane);
      panel.add(createButtonsAndRefList());
      
      return panel;
  }
  //___________________________________________________________________

  private JPanel createButtonsAndRefList()
  {
	  JPanel panel = new JPanel();
	  panel.setLayout(new BoxLayout(panel,BoxLayout.Y_AXIS));
	  panel.add(createButtons());
	  
	  fLastPulseLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
	  panel.add(fLastPulseLabel);
	  panel.add(createNewRefTextInp());
	  
	  panel.add(createRefList(),BorderLayout.SOUTH);
	  return panel;
  }
  
  private JPanel createButtons()
  {
	  JPanel panel = new JPanel(new FlowLayout());
	  
      panel.add(createButton("Save", ComponentIDs.SaveButton));
      panel.add(createButton("Retrieve", ComponentIDs.RetrieveButton));
	  
      panel.setAlignmentX(Component.LEFT_ALIGNMENT);
	  return panel;
  }
  //___________________________________________________________________
  
  private JPanel createNewRefTextInp()
  {
	  JPanel panel = new JPanel(new FlowLayout());
	  
      panel.add(fNewRefLabel, FlowLayout.LEFT);
      
      fNewReferenceName = new JTextField();
      fNewReferenceName.setPreferredSize( new Dimension( 200, 24 ) );

      panel.add(fNewReferenceName);
      
      panel.add(createButton("Append \nLSA cycle name", ComponentIDs.AppendLsaCycleName));
      
      panel.setAlignmentX(Component.LEFT_ALIGNMENT);
	  return panel;
  }
  //___________________________________________________________________
  
  private JButton createButton(String title, ComponentIDs ide)
  {
      JButton button = new JButton(title);
      button.setSize(300, 100);
      button.addActionListener(this);

      LOGGER.info("createMainTab: setting up Button: name {} id {} ", ide.name(), ide.ordinal());
      button.putClientProperty("id", ide.ordinal());
      return button; 
	  
  }
  //___________________________________________________________________
  private JScrollPane createRefList()
  {
      String columns[]={"Reference","Date","Comment"};
      String data[][]={ {"-","-","-"}};   
      
      fReferencesTableModel  = new DefaultTableModel(columns, 1);
      fReferencesTable = new JTable(fReferencesTableModel);
      fReferencesTable.setBounds(30,40,200,300);          
      JScrollPane sp=new JScrollPane(fReferencesTable);    

      return sp;
  }
  //___________________________________________________________________

  public void setBeamLabel(String text, long cycleTimeStamp)
  {
	  fLastPulseCycleTimestamps.addFirst(cycleTimeStamp);
	  if (fLastPulseCycleTimestamps.size() > 10)
	  {
		  fLastPulseCycleTimestamps.removeLast();
	  }
	  String datestr = fTimeFmt.format(Date.from(Instant.ofEpochSecond(0, cycleTimeStamp)));
	  fLastPulseLabel.setText(text+" "+ datestr);

  }
  //_______________________________________________________________________
  protected Integer createReferenceTab()
  {
	  int row = fReferencesTable.getSelectedRow();
	  if (row < 0)
	  {
		  JOptionPane.showMessageDialog(this, "Please select reference in table below");
		  return 0;
	  }
	  String selectedRefName = (String)fReferencesTableModel.getValueAt(row, 0);
	  
	  if (selectedRefName.startsWith("<"))
	  {
		  int answer = JOptionPane.showConfirmDialog(null, "The reference name "+selectedRefName+" looks invali. Retrieve anyway?","Error", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
		  if (answer == JOptionPane.NO_OPTION)
		  {
		    return 0;
		  }
	  }
	  

	  Reference ref = new Reference(selectedRefName);
	  Integer n = ref.retrieve();
	  
	  if (n > 0)
	  {
		ReferenceViewPanel refView = new ReferenceViewPanel(ref, fSelectedTgmUser, fCycleMapping.get(fSelectedTgmUser));
	    fTabs.addTab(selectedRefName,null, refView.getPanel(),"Reference Manager");
	    fOpenReferences.add(refView);
	    
	  }
	  else
	  {
		  JOptionPane.showMessageDialog(this, "The selected reference is empty, nothing to show.");
	  }
	  
	  return n;
  }
  //_______________________________________________________________________

  @Override
  public void windowOpened(WindowEvent e) {
	  // TODO Auto-generated method stub

  }
  @Override
  public void windowClosing(WindowEvent e) {
	  // TODO Auto-generated method stub

  }
  @Override
  public void windowClosed(WindowEvent e) {
	  System.out.println("Frame "+ e.getWindow().getName() + " closed");
  }
  @Override
  public void windowIconified(WindowEvent e) {
	  // TODO Auto-generated method stub

  }
  @Override
  public void windowDeiconified(WindowEvent e) {
	  // TODO Auto-generated method stub

  }
  @Override
  public void windowActivated(WindowEvent e) {
	  // TODO Auto-generated method stub

  }
  @Override
  public void windowDeactivated(WindowEvent e) {
	  // TODO Auto-generated method stub

  }
  @Override
  public void valueChanged(TreeSelectionEvent e) {
	  // TODO Auto-generated method stub

  }
  @Override
  public void actionPerformed(ActionEvent e) {

	  System.out.println("Action!");
	  System.out.println(e);
	  System.out.println("Event id = " + e.getID());

	  int i = 0;
	  for ( ComponentIDs v: ComponentIDs.values())
	  {
		  System.out.println("Component id "+i+" "+ v);
		  i++;
	  }


	  int buttonId = (int) ((JButton)e.getSource()).getClientProperty( "id" );

	  ComponentIDs whichAction = ComponentIDs.values()[buttonId];

	  switch (whichAction) 
	  {
	  case SaveButton:
		  
		  saveReference();
		  break;
	  case RetrieveButton:
		  retrieveReference();
		  break;
	  case AppendLsaCycleName:
		  appendLsaCycleNameToRefName();
		  break;

	  }

  }

  public void saveReference()
  {
	  System.out.println("Save reference");
	  String lsaUser = fCycleMapping.get(fSelectedTgmUser);
	  String newTag = fNewReferenceName.getText();
	  
	  // panel showing which recent pulse should be tagged
	  CycleSelectionPanel panel = new CycleSelectionPanel(fLastPulseCycleTimestamps, newTag, lsaUser);

  }

  public void retrieveReference()
  {
	  System.out.println("MainPanel::retrieveReference");
	  LOGGER.info("MainPanel::retrieveReference");
	  
	  CompletableFuture.supplyAsync(() -> createReferenceTab());

  }
  //___________________________________________________________
  public void appendLsaCycleNameToRefName()
  {
	  if (fSelectedTgmUser.isBlank())
	  {
		  return;
	  }

	  String lsaCycleName = fCycleMapping.get(fSelectedTgmUser);

	  String currentText = fNewReferenceName.getText();

	  fNewReferenceName.setText(currentText+"_"+lsaCycleName);

  }
  //___________________________________________________________

  public Map<String,String> getCycleMapping()
  {
	  return fCycleMapping;
  }

}
