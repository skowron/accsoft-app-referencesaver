package cern.accsoft.op.referencesaver.gui.treeobjects;

import java.awt.Color;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import org.codehaus.janino.Java.NewAnonymousClassInstance;

import cern.japc.value.MapParameterValue;
import cern.nxcals.api.domain.Entity;


/**
 * Wrapper around Entity object of NXCALs so we can insert it to JTree branches
 * 	
 */
public class EntityParam  implements Comparable<Object> {
	
	Entity fEntity = null;
	Type     fType = Type.UNDEFINED;
	Status fStatus = Status.NON_RETRIEVED;
	Map<String, String> fFields = new TreeMap<String,String>();
	
	TreeMap<Date, MapParameterValue> fBufferedValues = new TreeMap<Date, MapParameterValue>();
	
	public enum Type
	{
      /** defines if this parameter is an acquisition type and a setting *
	  */
		/**
		 * Not determined yet
		 */
		UNDEFINED(Color.pink),
		/**
		 * setting, present in LSA, has trim history 
		 */
		SETTING_LSA(Color.green),
		/**
		 * Looks like setting, no CYCLE STAMP and logged only with ACQUISITION STAMP, 
		 * but not present at every pulse, see ACQUISITION_NON_CYCLEBOUND
		 */
		SETTING_NON_LSA(Color.cyan),		
		/**
		 * Doesn't have cycle stamp, has acquisition at (almost) every pulse in between start Cycle and end Cycle 
		 */
		ACQUISITION_NON_CYCLEBOUND(Color.magenta), 
		/**
		 * Has cycle stamp
		 */
		ACQUISITION_CYCLEBOUND(Color.white);
		
		Color fBgColor;
		
		Type(Color bgcol)
		{
			fBgColor = bgcol;
		}
		
		public Color getBgColor()
		{
			return fBgColor;
		}

	}
	
	public enum Status
	{
		NON_RETRIEVED("/icons/Button_nonRetrieved.png"),
		FIELDS_RETRIEVED("/icons/Button_nonRetrieved.png"),
		RETRIEVING("/icons/Button_clock.png"),
		RETRIEVED("/icons/Button_retrieved.png"),
		ERROR("/icons/Button_red_card.png"),
		NON_SUPPORTED("/icons/Button_white_card.png");
		
		Status(String iconPath)
		 {
			URL clockiconurl = getClass().getResource(iconPath);
	 	    fIcon = new ImageIcon(clockiconurl);
		 }
		
		Icon fIcon;
		
		public Icon getIcon()
		{
			
			return fIcon;
		}
		
	}
	
	//_____________________________________________________________________________________________
	
	public EntityParam(Entity entity)
	{
	  fEntity = entity;	
	  
	}
	
	//
	public void setFields(Map<String, String> fields)
	{
		fFields.clear();
		fFields.putAll(fields);
	}
	//
	public Map<String, String> getFields()
	{
		return fFields ;
	}
	
	//_____________________________________________________________________________________________
	
	public Entity getEntity()
	{
	  return fEntity;
	}
	//_____________________________________________________________________________________________

	public Type getType()
	{
	   return fType;
	}
	//_____________________________________________________________________________________________
	
	public Status getStatus()
	{
	  return fStatus;
	}
	//_____________________________________________________________________________________________
	
	public void setStatus(Status status)
	{
	  fStatus = status;
	}
	//_____________________________________________________________________________________________
	
	public void setType(Type type)
	{
	  fType = type;
	}
	//_____________________________________________________________________________________________
	
	public void setTypeAcquisitionCB()
	{
	  fType = Type.ACQUISITION_CYCLEBOUND;
	}
	//_____________________________________________________________________________________________
	
	public void setTypeAcquisitionNonCB()
	{
	  fType = Type.ACQUISITION_NON_CYCLEBOUND;
	}
	//_____________________________________________________________________________________________
	
	public void setTypeSettingLsa()
	{
	  fType = Type.SETTING_LSA;
	}
	//_____________________________________________________________________________________________
	
	public void setTypeSettingNonLsa()
	{
	  fType = Type.SETTING_NON_LSA;
	}
	//_____________________________________________________________________________________________
	
	public String getDeviceName()
	{
	  if (fEntity == null) return "";
	  
	  Map<String, Object> keyvals = fEntity.getEntityKeyValues();
	  return (String) keyvals.get("device");
	}
	//_____________________________________________________________________________________________

	public String getPropertyName()
	{
	  if (fEntity == null) return "";
	  
	  Map<String, Object> keyvals = fEntity.getEntityKeyValues();
	  return (String) keyvals.get("property");
	}
	//_____________________________________________________________________________________________
	
	public String getParameterName()
	{
	  if (fEntity == null) return "";
	  
	  Map<String, Object> keyvals = fEntity.getEntityKeyValues();
	  return (String) keyvals.get("device") +"/"+ (String) keyvals.get("property");
	}
	//_____________________________________________________________________________________________
	
	public String toString()
	{
	  if (fEntity == null) return "";
	  
	  Map<String, Object> keyvals = fEntity.getEntityKeyValues();
	  return (String) keyvals.get("property");
	}
	//_____________________________________________________________________________________________
	
	public void addBufferedValues(TreeMap<Date, MapParameterValue> bufferedValues)
	{
		fBufferedValues.putAll(bufferedValues);
	}

	//_____________________________________________________________________________________________
	
	public void addBufferedValue(Date date, MapParameterValue val)
	{
		fBufferedValues.put(date, val);
	}
	
	//_____________________________________________________________________________________________

	public TreeMap<Date, MapParameterValue>  getBufferedValues()
	{
		return fBufferedValues;
	}

	@Override
	public int compareTo(Object o) {
		if (o == null) return 1;
		return toString().compareTo(o.toString());
	}

}
