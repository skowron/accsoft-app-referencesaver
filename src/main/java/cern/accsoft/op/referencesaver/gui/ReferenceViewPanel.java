package cern.accsoft.op.referencesaver.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.Map.Entry;
import java.util.concurrent.CompletableFuture;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cern.accsoft.gui.frame.MessageManager;
import cern.accsoft.op.referencesaver.Config;
import cern.accsoft.op.referencesaver.ReferenceSaverGuiApp;
import cern.accsoft.op.referencesaver.gui.treeobjects.EntityParam;
import cern.accsoft.op.referencesaver.gui.treeobjects.EntityParam.Status;
import cern.accsoft.op.referencesaver.gui.treeobjects.EntityParam.Type;
import cern.accsoft.op.referencesaver.gui.treeobjects.GroupEntityParam;
import cern.accsoft.op.referencesaver.nxcals.Cache;
import cern.accsoft.op.referencesaver.nxcals.Extractor;
import cern.accsoft.op.referencesaver.nxcals.Reference;
import cern.accsoft.op.referencesaver.nxcals.TrimHistory;
import cern.accsoft.op.referencesaver.plotters.Plotter;
import cern.accsoft.op.referencesaver.plotters.ScalarPlotter;
import cern.japc.value.MapParameterValue;
import cern.japc.value.SimpleParameterValue;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.TimeWindow;
import lombok.NonNull;

public class ReferenceViewPanel   {
   /**
    *  Object that handles retrieving and plotting data for particular reference.
    *  It is made so the user needs to call 3 methods to build the panel
    *  1. Constructor
    *  2. retrieve: this will retrieve the Entities for the reference and will return its number
    *  3. getPanel will create graphics with tree all entities. No point to call it if retrieve returned 0.
    *    
    */
	
	protected final Logger LOGGER = LoggerFactory.getLogger(getClass());
	
	JPanel fPanel;
	JPanel fCentralPanel;
	JLabel fLabel;
	/**
	 * Map of entities for each devices
	 * reminder: entity means property, but in nxcals lingo
	 */
	TreeMap<String, LinkedList<EntityParam>> fEntityParams = null; 
	Plotter fPlotter;
	
	List<Reference> fReferences = new LinkedList<Reference>();
	String fTgmUserName = null;
	String fLsaCycleName = null;
	
	TreeCreator fTreeCreator = null;
    //_________________________________________________________________
	
	
	public ReferenceViewPanel(Reference reference, String tgmUserName, String lsaCycleName)
	{
		super();
		
		fReferences.add(reference);
		fTgmUserName = tgmUserName;
		fLsaCycleName = lsaCycleName;
		
		fTreeCreator = new FlatTreeCreator(this);
		
		//fTreeCreator = new ClassTreeCreator(this);
	}
    //_________________________________________________________________
	
	public List<Reference> getReferences()
	{
		return fReferences;
	}
	protected Reference getReference()
	{
		return getReferences().get(0);
	}
	//_____________________________________________________________________
	
	public TreeMap<String, LinkedList<EntityParam>> getEntityParams()
	{
		if ( fEntityParams != null )
		{
			return fEntityParams;
		}
		
		fEntityParams = new TreeMap<String, LinkedList<EntityParam>>();
		
		Set<Entity> entities = getReference().getEntities();

		
		fEntityParams = new TreeMap<String, LinkedList<EntityParam>>();
		
		Map<String, List<String>> dpfs = Cache.Instance().readDevPropFields();
		
	    for (Entity ent : entities)
		{
			@NonNull
			Map<String, Object> keyvals = ent.getEntityKeyValues();
				
			Instant inst = getReference().getTag().getTimestamp();
			String devname = (String)keyvals.get("device");
			
			// skowron: tmp solution to reduce number of devices during development 
			if ( Config.isDev() && !(devname.contains("BPM") || devname.contains("ACAVLOOP")))
				continue;
			
			
			EntityParam ep = new EntityParam(ent);
			
			Map<String, String> fields = null;
			try
			 {
			   fields = getFileds(devname, ep.getPropertyName(), dpfs, inst);
			   ep.setFields(fields);
			   ep.setStatus(Status.FIELDS_RETRIEVED);
			 }
			catch(Throwable e)
			{
				LOGGER.error("Cannot determine fields for {}", ep.getParameterName());
				// no problem, will determine fileds when extracting data, if user needs it
			}
			
			
			fEntityParams.computeIfAbsent((String)keyvals.get("device"), x -> new LinkedList<EntityParam>())
					  .add(ep);
			
		}
		
	    if (dpfs == null)
	    {
	    	Cache.Instance().saveDevPropFields(getFieldsForCache());
	    }
	    
		return fEntityParams;
	}
	
	/**
	 * Converts the map of EntityParams to one containing list if fields with their type
	 * fielName:typeName so it can be save in cache file 
	 * @return
	 */
	protected Map<String, List<String>> getFieldsForCache()
	{
		TreeMap<String,List<String>> classmap = new TreeMap<String, List<String>>();
		
		for (String dev : fEntityParams.navigableKeySet())
		{
	   	   
			LinkedList<EntityParam> props = fEntityParams.get(dev);
			for (EntityParam ep: props)
			{
				
				LinkedList<String> l = new LinkedList<String>();
				for (Entry<String, String> fieldent: ep.getFields().entrySet())
				{
					
					l.add( fieldent.getKey() +":"+ fieldent.getValue());
				}
				
				classmap.put(ep.getParameterName(), l);
			}
		}
		return classmap;
	}
	//_______________________________________________________________________
	//
	protected Map<String, String> getFileds(String dev, String prop, Map<String, List<String>> cache,  Instant inst)
	{
		if (cache == null)
		{
			Extractor extractor = ReferenceSaverGuiApp.getInstance().getExtractorService();
			return extractor.getFields(dev,prop, inst);
		}
		
		String devprop = dev+"/"+prop;
		List<String> fields = cache.get(devprop);
		
		if (fields == null)
		{
			Extractor extractor = ReferenceSaverGuiApp.getInstance().getExtractorService();
			return extractor.getFields(dev,prop, inst);
		}
		
		Map<String,String> retval = new TreeMap<String, String>();
		for (String fieldtype: fields)
		{
			String[] els = fieldtype.split(":");
			if (els.length !=2 )
			{
				Extractor extractor = ReferenceSaverGuiApp.getInstance().getExtractorService();
				return extractor.getFields(dev,prop, inst);
			}
			retval.put(els[0], els[1]);
		}
		return retval;
	}
	//_______________________________________________________________________
	//
	public EntityParam getEntityParam(String deviceName, String propertyName)
	{
		
		if (fEntityParams == null || propertyName == null)
		{
			return null;
		}
		
		LinkedList<EntityParam> props = fEntityParams.get(deviceName);
		
		return props.stream().filter(f -> propertyName.equals(f.getPropertyName())).findAny().orElse(null);
	}
	//_______________________________________________________________________
	public JPanel getPanel()
	{
	   if (fPanel != null)
	   {
		   return fPanel;
	   }
      
	   
	   if (getReference().getEntities() == null)
		{
		   JOptionPane.showMessageDialog(null, "No entities to show", "Error", JOptionPane.ERROR_MESSAGE, null);	
		   return null;
		}
	   fPanel = new JPanel(new BorderLayout());
	   
	   JComponent ftreecont = null;
	   try
	   {
	      ftreecont =  createTree();
	   }
	   catch(Exception ex)
	   {
		   System.err.println("ReferenceViewPanel::getPanel: EXCEPTION "+ex.getMessage());
		   ex.printStackTrace();
		   return null;
		   
	   }
	   
	   fLabel = new JLabel("Reference "+getReference().getName()+" : "+ getReference().getEntities().size() +" entities");


	   fPanel.add(ftreecont, BorderLayout.WEST);
	   fPanel.add(fLabel, BorderLayout.NORTH);
	   
	   
	   fCentralPanel = new JPanel(new BorderLayout());
	   fPanel.add(fCentralPanel, BorderLayout.CENTER);
	   
	   return fPanel;
	   
	}
	//_______________________________________________________________________
	
	protected JComponent createTree()
	{
		return fTreeCreator.create();
	}

	//_______________________________________________________________________
	public void finish() 
	{
	/**
	 * closes the subscriptions
	 */
		// let know the garbage collector that we do not use it it anymore
		fPanel = null;
		fCentralPanel = null;
		fLabel = null;
				
	}


	//_________________
	public int fillInDetailPanel(String device, String parameter, JTextArea textarea)
	{
		System.out.println("showRefDetailPanel::showRefDetailPanel"+ device+"/"+parameter);
		StringBuilder sb =  new StringBuilder();
		
		for (Reference ref: fReferences)
		{
			sb.append("Reference Name : "+ref.getName()+"\n");
			
			Entity ent = ref.getEntityByDevPropNames(device,parameter);
			List<MapParameterValue> values = getReference().getValues(ent);
			int i=1;
			for (MapParameterValue mpv: values)
			{
				sb.append("    Value "+i+" of "+values.size()+"\n");
				for (String field: mpv.getNames())
				{
					sb.append("        "+field+" : "+ mpv.get(field));
					
					sb.append("\n");
				}
				
			}
			sb.append("---------------------------------------------------------\n");
			
		}
		
		
		SwingUtilities.invokeLater(() -> {textarea.setText(sb.toString()); } );
		
		return 0;
	}
    //_________________________________________________________________

	public void showRefDetailPanel(TreePath tPath)
	{
		System.out.println("showRefDetailPanel::showRefDetailPanel"+ tPath);
		if (tPath.getPathCount() < 3)
		{
			return;
		}
			
		JFrame secondFrame = new JFrame("Reference datails "+tPath );
		secondFrame.setPreferredSize(new Dimension(640,480));
		
		secondFrame.setLayout(new BorderLayout());
		JLabel label = new JLabel("Reference details for "+tPath);
		secondFrame.add(label,BorderLayout.NORTH);
		
		DefaultMutableTreeNode node2 = (DefaultMutableTreeNode) tPath.getPathComponent(2);
	    EntityParam ep = (EntityParam) node2.getUserObject();
	    
	    JTextArea jtext = new JTextArea("Retrieving data from NXCALS ...");
	    JScrollPane jscrolltext = new JScrollPane(jtext);   
	    jscrolltext.setFont(jscrolltext.getFont().deriveFont(12));
	    secondFrame.add(jscrolltext, BorderLayout.CENTER);
		
	    secondFrame.pack();
		secondFrame.setVisible(true);

	    CompletableFuture.supplyAsync(() -> fillInDetailPanel(ep.getDeviceName(),ep.getPropertyName(), jtext));
		
		
	}


    //_________________________________________________________________

	protected TreeMap<Date, MapParameterValue> readLoggedData(EntityParam ep, String field, ScalarPlotter plotter)
	{
		
		TreeMap<Date, MapParameterValue> buffered = ep.getBufferedValues();
		Instant tFrom = null;
		Instant now = Instant.now();
	    if (buffered == null || buffered.isEmpty())
	     {
	    	tFrom = Instant.now().minus(Duration.ofDays(1));
	     }
	    else
	    {
	    	tFrom = buffered.lastKey().toInstant();
	    }


		
		Extractor extr = ReferenceSaverGuiApp.getInstance().getExtractorService();
		
		
		System.out.println("ReferenceViewPanel::showValue: Extracting data from NXCALS from "+ tFrom + " to "+ now);
		TreeMap<Date, MapParameterValue> mpvs = extr.getValues(ep.getDeviceName(), ep.getPropertyName(), fLsaCycleName, TimeWindow.between(tFrom,now));
		System.out.println("ReferenceViewPanel::showValue: got "+mpvs.size()+" values");
	    ep.setType(	extr.getLastValuesType() );
	    
	    if (ep.getType() == Type.SETTING_NON_LSA || ep.getType() ==  Type.UNDEFINED)
	    {
	    	System.out.println("ReferenceViewPanel::showValue: checking trims ");
	    	
	    	TrimHistory trimExtr = ReferenceSaverGuiApp.getInstance().getTrimHistoryService();
	    	TreeMap<Date, MapParameterValue> trims = trimExtr.getTrims(ep.getDeviceName()+"/"+ ep.getPropertyName(), fLsaCycleName );
	    	if (!trims.isEmpty())
	    	{
	    	   ep.setType(Type.SETTING_LSA);
	    	   
	    		   mpvs = new TreeMap<Date, MapParameterValue>();
	    		   
	    		   Date knewest = trims.descendingKeySet().iterator().next();
	    		   MapParameterValue vnewest = trims.get(knewest);
	    		   mpvs.put(knewest, vnewest);
	    	}
	    	else
	    	{
		    	System.out.println("ReferenceViewPanel::showValue: No Trims  ");

	    	}
	    }
	    
	    
		double t[] = new double[mpvs.size()];
		double y[] = new double[mpvs.size()];
		int i=0;
		for (Date d: mpvs.navigableKeySet())
		{
			t[i] = d.toInstant().toEpochMilli();
			y[i] = mpvs.get(d).getDouble(field);
			i++;
		}

	    plotter.addData(t, y);
	    
	    ep.addBufferedValues(mpvs);
	    return mpvs;
	}
    //_________________________________________________________________
	
	public int showValuesAndTheirReferences( GroupEntityParam ep, String field )
	{
		System.out.println("*******************************************************");
		System.out.println("*******************************************************");
		System.out.println("*******************************************************");
		
		return 0;
	}
	
	//_________________________________________________________________
	public int showValueAndItsReference( EntityParam ep, String field )
	{
		System.out.println("*******************************************************");
		
	    
	    String fullParameterName = ep.getParameterName() +"#"+ field;
		if (fPlotter != null)
		{
			if ( fullParameterName.equals(fPlotter.getParameterName()) )
			 {
				System.out.println("We are already plotting this parameter");
				return 0 ;
			 }
			
			System.out.println("Destroying old plotter for "+fPlotter.getParameterName());
			fPlotter.finish();
			fCentralPanel.remove(fPlotter);
			fPlotter = null;
			fCentralPanel.revalidate();
			fCentralPanel.repaint();
			System.out.println("-------------------------------------------------------");
			System.out.println("-------------------------------------------------------");
			System.out.println("-------------------------------------------------------");
			
		}	    
	    TreeMap<Date, MapParameterValue> mpvs = ep.getBufferedValues();
    	MapParameterValue mpv = getReference().getValues(ep.getEntity()).get(0); // if more than one value value (what is strange, meaning multiple aqns in a cycle), then we use the 1st one 
    	SimpleParameterValue refvalue = mpv.get(field);

		System.out.println("ReferenceViewPanel::showValue " + refvalue);
		
		if (refvalue.getValueType().isNumeric() || refvalue.getValueType().isBoolean() && refvalue.getValueType().isScalar())
		{
			fCentralPanel.removeAll(); 
			

		    System.out.println("ttttttttttttttttttttttttttttttttttttttttttttttttttttttt");
		    System.out.println("ReferenceViewPanel::showValue " + refvalue+" has "+mpvs.size()+ " recent values for "+ep.getParameterName());
		    System.out.println("Type is "+ep.getType()+" Status is "+ ep.getStatus());
		    System.out.println("ttttttttttttttttttttttttttttttttttttttttttttttttttttttt");

		    
		    System.out.println("t1");
			
		    
		  
		    
		    System.out.println("t2");
			double t[] = new double[mpvs.size()];
			double y[] = new double[mpvs.size()];
			int i=0;
			for (Date d: mpvs.navigableKeySet())
			{
				t[i] = d.toInstant().toEpochMilli();
				y[i] = mpvs.get(d).getDouble(field);
				i++;
			}
			System.out.println("t3");
			try 
			{
				fPlotter = new ScalarPlotter(this, fullParameterName, fLsaCycleName, 2000, t,y);
				System.out.println("t4");
			} 
			catch (Throwable e) 
			{
				System.out.println("t5");
				e.printStackTrace();

				MessageManager.error("Could not instantiate ScalarPlotter for " + ep.getParameterName(), e, this);
				return 1;

			}
			
			CompletableFuture.supplyAsync(() -> readLoggedData(ep, field, (ScalarPlotter)fPlotter));
		    
			System.out.println("t6");
			fCentralPanel.add(fPlotter, BorderLayout.CENTER);
			System.out.println("t7");
			fCentralPanel.revalidate();
			fCentralPanel.repaint();
			System.out.println("ReferenceViewPanel::showValue: added new plotter ");
			System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++");
			System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++");
			System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++");
			
		}
		
		return 0;
	}
}
