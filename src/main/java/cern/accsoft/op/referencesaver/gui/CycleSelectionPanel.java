package cern.accsoft.op.referencesaver.gui;



import java.awt.Dimension;
import java.awt.event.WindowEvent;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import cern.accsoft.op.referencesaver.Config;
import cern.accsoft.op.referencesaver.ReferenceSaverGuiApp;
import cern.accsoft.op.referencesaver.co.LsaUtils;
import cern.accsoft.op.referencesaver.nxcals.Tagging;
import cern.nxcals.api.custom.domain.Tag;

public class CycleSelectionPanel {
    
	String fTag = "";
	String fLsaCycle = "";
	Boolean fShowTable = true;
	protected SimpleDateFormat fTimeFmt = new SimpleDateFormat("yyyy.MM.dd H:m:s");
	
	public CycleSelectionPanel(List<Long> timestamps, String newTag, String lsaCycle) 
	{
		
		if (newTag == null || newTag.isBlank())
		{
			JOptionPane.showMessageDialog(null, "You need to specify reference name");
			
			return;
		}
		Tagging tagSrv = ReferenceSaverGuiApp.getInstance().getTaggingService();
		
		if (tagSrv.isTagExisting(newTag))
		{
			Tag tag = tagSrv.getTag(newTag);
			JOptionPane.showConfirmDialog(null, "Such tag already exists: \n"+tag.toString(),"Error",JOptionPane.OK_OPTION, JOptionPane.ERROR_MESSAGE);
			return;
		}

		fTag = newTag;
		fLsaCycle = lsaCycle;
		
		
		if (fShowTable)
		{
			SwingUtilities.invokeLater(() -> createWindow(timestamps));
		}
		else
		{
		 Long s = (Long)JOptionPane.showInputDialog(
			    null,
			    "Select pulse",
			    "Pulse Selection",
			    JOptionPane.QUESTION_MESSAGE,
			    null, //  new ImageIcon(...
				  // les possibilités 
			    timestamps.toArray(Long[]::new), 
			    timestamps.get(0));// valeur initiale
         
		
		if (s == null)
		{
			return;
		}
		
		CompletableFuture.supplyAsync(() -> saveReference(s));

		}
    }
	
	protected void createWindow(List<Long> timestamps)
	{
        JFrame frame = new JFrame("Item Selection");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setAlwaysOnTop(true);

        
        //String[] items =  timestamps.stream().map(String::valueOf).toArray(String[]::new);
        //JComboBox<String> comboBox = new JComboBox<>(items);
         

        
        String columns[]={"timestamp","Date","Destination"};
        DefaultTableModel fPulseTableModel = new DefaultTableModel(columns,0);
        for (Long ts: timestamps)
        {
           String[] noTrims =  new String[] {ts.toString(),fTimeFmt.format(Date.from(Instant.ofEpochSecond(0, ts))),"-"};
           fPulseTableModel.addRow( noTrims );
        }

        JTable fPulseTable = new JTable(fPulseTableModel);
        JScrollPane sp=new JScrollPane(fPulseTable);
        

        JButton buttonSave = new JButton("Save");
        buttonSave.addActionListener(e -> {
            int rowno = fPulseTable.getSelectedRow();
            //fPulseTableModel.getValueAt(rowno, 0);
            Long selectedtimestamp = timestamps.get(rowno);
            
            CompletableFuture.supplyAsync(() -> saveReference(selectedtimestamp));
            frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
            
        });
        JButton buttonCancel = new JButton("Cancel");
        buttonCancel.addActionListener(e -> {
            frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
        });

        buttonCancel.setPreferredSize(new Dimension(150, 40));
        buttonSave.setPreferredSize(new Dimension(150, 40));
        
        JPanel buttonpanel = new JPanel();
        buttonpanel.add(buttonCancel);
        buttonpanel.add(buttonSave);

        frame.add(sp);
        frame.add(buttonpanel,"South");


        frame.setSize(500, 300);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        
	}
	protected Boolean saveReference(Long ts)
	{
		Tagging tagSrv = ReferenceSaverGuiApp.getInstance().getTaggingService();
		
		try
		{
	      tagSrv.createTag(fTag, ts);
		}
		catch(Exception ex)
		{
		   System.err.println("Created tag threw exception!");
		   ex.printStackTrace();
		   return false;
		}
	    
	    LsaUtils.trimSetting(Config.getReferenceNameParam(), fLsaCycle, fTag);// this one is with comment
	    
        //JapcUtils.setNewReference(newTag, tgmUser);
	    
    	SwingUtilities.invokeLater(new Runnable()
    	{
    	   public void run()
    	   {
    		   ReferenceSaverGuiApp.getInstance().getMainPanel().updateListOfReferences();
    		   
    	   }
    	});
		
    	return true;
	}
	protected void saveReferenceAsynch(Long ts)
	{
		
		Runnable runnable = () -> {saveReference(ts);};
		Thread thread = new Thread(runnable);
		thread.start();
	}
	 
}
