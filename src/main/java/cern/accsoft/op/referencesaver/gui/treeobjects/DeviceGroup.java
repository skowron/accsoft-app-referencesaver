package cern.accsoft.op.referencesaver.gui.treeobjects;

import java.util.LinkedList;
import java.util.List;

/**
 * Object that it attached to JTree branch representing group of devices of the same class
 * It is assumed that all devices are of the same class   
 */
public class DeviceGroup {

	String fName = "";
	
	List<String> fDeviceNames = new LinkedList<String>();
	
	public DeviceGroup(String name, List<String> deviceNames)
	{
		fName = name;
		fDeviceNames = deviceNames;
	}
	
	public List<String> getDeviceNames()
	{ 
		return fDeviceNames;
	}

	public String getName()
	{
		return fName;
	}
	
	public String toString()
	{
		return fName;
	}
}
