package cern.accsoft.op.referencesaver.gui.treeobjects;

import java.util.LinkedList;
import java.util.List;

/**
 * Holds the entities for the same parameter of each device
 */
public class GroupEntityParam  {

	DeviceGroup fDeviceGroup;
	List<EntityParam> fEntities  = new LinkedList<EntityParam>();
	
	public GroupEntityParam(EntityParam ep, DeviceGroup devGroup) {
		
		fEntities.add(ep);
		fDeviceGroup = devGroup;
	}

	public GroupEntityParam(List<EntityParam> lep, DeviceGroup devGroup) {
		fEntities.addAll(lep);
		fDeviceGroup = devGroup;
	}
    
	public List<EntityParam> getEntities()
	{
		return fEntities;
	}
	
	public DeviceGroup getDeviceGroup()
	{
		return fDeviceGroup;
	}
	
	public String toString()
	{
		if (fEntities == null || fEntities.isEmpty())
		{
			return "<EMPTY ENTITY>";
		}
		
		return fEntities.get(0).getPropertyName();
	}
}
