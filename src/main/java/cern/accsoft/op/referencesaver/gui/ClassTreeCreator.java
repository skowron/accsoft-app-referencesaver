package cern.accsoft.op.referencesaver.gui;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Pattern;

import javax.swing.JComponent;
import javax.swing.tree.DefaultMutableTreeNode;

import cern.accsoft.ccs.ccda.client.model.accelerator.Accelerator;
import cern.accsoft.commons.ccs.CcdaUtils;
import cern.accsoft.op.referencesaver.ReferenceSaverGuiApp;
import cern.accsoft.op.referencesaver.gui.treeobjects.DeviceGroup;
import cern.accsoft.op.referencesaver.gui.treeobjects.EntityParam;
import cern.accsoft.op.referencesaver.gui.treeobjects.Field;
import cern.accsoft.op.referencesaver.gui.treeobjects.GroupEntityParam;
import cern.accsoft.op.referencesaver.nxcals.Reference;

/**
/RefName
   \
   GroupName1
   |        \ 
   |         Prop1
   |         |    \   
   |         |     Field1   
   |         |    \   
   |         |     Field2   
   |         Prop2
   |         |    \   
   |         |     Field3   
   |         |    \   
   |         |     Field4   
   |         Prop3 
   |         |    \   
   |         |     Field5   
   |         |    \   
   |         |     Field6
   |         AllDevs 
   |         |    \   
   |         |     Dev1   
   |         |     |    Prop1
   |         |     |         \   
   |         |     |           Field1   
   |         |     |           Field2   
   |         |     |    Prop2
   |         |     |         \   
   |         |     |           Field3   
   |         |     |           Field4   
   |         |     |    Prop3
   |         |     |         \   
   |         |     |           Field5   
   |         |     |           Field6   
      
*/
   
public class ClassTreeCreator extends TreeCreator{

	public ClassTreeCreator(ReferenceViewPanel refViewer)
	{
		super(refViewer);
	}
	
	/**
	 * Checks that for any of these devices there is some entity
	 * To avoid creating empty branches. 
	 * @param devs
	 * @return
	 */
	public boolean isNoEntitiesPresentForDevices(List<String> devs)
	{
		for (String dev: devs)
		{
			if (fRefViewer.getEntityParams().containsKey(dev)) 
		     {
				return false; 
			 }
		}
		return true;
	}
	
	public EntityParam findEntityParamForDevice(String device, String searchedProperty)
	{
		if (device == null)
		{
		   System.err.println("findEntityParamForDevice: device is NULL");
		   return null;	
		}
		if (searchedProperty == null)
		{
		   System.err.println("findEntityParamForDevice: searchedProperty is NULL");
		   return null;	
		}

		
		LinkedList<EntityParam> eps = fRefViewer.getEntityParams().get(device);
		if (eps == null)
		{
		   return null;	
		}
		
		EntityParam ep = eps.stream().filter(f -> f.getPropertyName().equals(searchedProperty)).findAny().orElse(null);
		return ep;
		
	}
	@Override
	public JComponent create() {
		// TODO Auto-generated method stub
		List<Reference> fReferences = fRefViewer.getReferences();
		
		String refname = fReferences.get(0).getName();
		DefaultMutableTreeNode root = new DefaultMutableTreeNode(refname);
		
		TreeMap<String, LinkedList<EntityParam>> entityParams = fRefViewer.getEntityParams();
		
		//map : device list for each group
		TreeMap<String, List<String>> classmap = createClassDevMap();
		 
		for( Entry<String, List<String>> group: classmap.entrySet() )
		{
			if (isNoEntitiesPresentForDevices(group.getValue()))
			{
				continue;
			}
			DeviceGroup devGroup = new DeviceGroup(group.getKey(), group.getValue());
			DefaultMutableTreeNode devGroupBranch = new DefaultMutableTreeNode(devGroup);
			
			
			//Find an EntityParam for one of devices in the group
			// it would be good to check that all of the are the same, or take the widest set
			
			List<EntityParam> props = null;
			int i=0;
			for (String devName: group.getValue() )
			{
			
				props = entityParams.get(devName);
				if ( props != null ) break;
			}
			
			if ( props == null ) continue;
			
			props.sort(null);
			
			for (EntityParam prop: props)
			{
				List<EntityParam> epForAllDevs = new LinkedList<EntityParam>(); 
				for (String deviceName: group.getValue())
				{
					//System.out.println(group.getKey()+" "+ prop.getParameterName() +" " + deviceName);
					
					
					EntityParam ep = findEntityParamForDevice(deviceName, prop.getPropertyName());
					
					if (ep == null)
					{
						//System.out.println("ClassTreeCreator::create: device "+ deviceName+ " of group "+group.getKey() +" was not saved in this reference");
						continue;
					}
					epForAllDevs.add(ep);
				}
				
				
				DefaultMutableTreeNode propBranch = new DefaultMutableTreeNode(new GroupEntityParam(epForAllDevs, devGroup));
				devGroupBranch.add(propBranch);
			}
			
			DefaultMutableTreeNode devListBranch = new DefaultMutableTreeNode("Devices");
			
			for (String deviceName: group.getValue())
			{
				DefaultMutableTreeNode devBranch = new DefaultMutableTreeNode(deviceName);
				props = entityParams.get(deviceName);
				if (props== null)
				{
					continue;
				}
				props.sort(null);
				for (EntityParam prop: props)
				{
					DefaultMutableTreeNode propBranch = new DefaultMutableTreeNode(prop);
					
					for (Entry<String, String> fieldent: prop.getFields().entrySet())
					{
						DefaultMutableTreeNode fieldNode = new DefaultMutableTreeNode(new Field(fieldent.getKey(),fieldent.getValue()));
						propBranch.add(fieldNode);
					}

					devBranch.add(propBranch);
				}

				
				devListBranch.add(devBranch);
			}
			
			
			devGroupBranch.add(devListBranch);
			
			System.out.println("Added "+group.getKey()+" with "+ devListBranch.getChildCount()+" devices");
			
			root.add(devGroupBranch);
			
		}

		return createTreeInPanel(root);
	}

	private void oldCreateClassDevMap()
	{
		TreeMap<String,List<String>> classmap = new TreeMap<String, List<String>>();  
		
		TreeMap<String, LinkedList<EntityParam>> entityParams = fRefViewer.getEntityParams();
		
		for (Entry<String, LinkedList<EntityParam>> ent: entityParams.entrySet())
		{
			EntityParam ep = ent.getValue().get(0);
			if (ep == null) continue;
			System.out.println( ep.getEntity().getEntityKeyValues().toString() );
			
			String devName = ent.getKey();
			String[] parts = devName.split(Pattern.quote("."));
			if (parts.length < 2) continue;
			
			classmap.computeIfAbsent(parts[1], x -> new LinkedList<String>()).add(devName);
			
			//entParam.getParameterName()
		}

	}
	
	public static TreeMap<String,List<String>> createClassDevMap()
	{
		TreeMap<String,List<String>> classmap = new TreeMap<String, List<String>>();  

		
		Accelerator accel = ReferenceSaverGuiApp.getInstance().getAccelerator();
		//String query = SearchBuilder.search().predicate(DeviceQueryField.accelerator, SearchOperator.EQUAL, accel.getName()).build();
		//List<DeviceNameAndAlias> devs = CcdaUtils.getDeviceService().searchDeviceNames(query);
		Set<String> classList = CcdaUtils.getDeviceClassNamesByAccelerator(accel.getName());
		String[] accels = new String[1];
		accels[0] = accel.getName();
		String[] classes = new String[1];
		
		
		for (String className: classList )
		{
			classes[0] = className;
			List<String> devs = new LinkedList<String>(CcdaUtils.getDeviceNamesByClassNamesAndAccelerators(accels, classes));
			classmap.put(className, devs);
			
		}
		
		//all devices that are single for a class move to dedicated group "Other" 
		List<String> others = classmap.computeIfAbsent("Other", l -> new LinkedList<String>());
		
		Iterator<Entry<String, List<String>>> it = classmap.entrySet().iterator();
		while (it.hasNext()) {
			Entry<String, List<String>> entry = it.next();
			if (entry.getValue().size() < 2)
			{
				
				others.addAll(entry.getValue());
				it.remove();
			}
		}
		
	   return classmap;	
		
	}
	
}
