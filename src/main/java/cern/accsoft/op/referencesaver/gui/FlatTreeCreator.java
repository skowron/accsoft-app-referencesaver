package cern.accsoft.op.referencesaver.gui;

import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.swing.JComponent;
import javax.swing.tree.DefaultMutableTreeNode;

import cern.accsoft.op.referencesaver.gui.treeobjects.EntityParam;
import cern.accsoft.op.referencesaver.gui.treeobjects.Field;
import cern.accsoft.op.referencesaver.nxcals.Reference;

public class FlatTreeCreator extends TreeCreator{
	
	
	public FlatTreeCreator(ReferenceViewPanel refViewer)
	{
		super(refViewer);
	}
	
	public JComponent create()
	{
		List<Reference> fReferences = fRefViewer.getReferences();
		
		String refname = fReferences.get(0).getName();
		DefaultMutableTreeNode root = new DefaultMutableTreeNode(refname);
		
		TreeMap<String, LinkedList<EntityParam>> fEntityParams = fRefViewer.getEntityParams();
		
		for (String dev : fEntityParams.navigableKeySet())
		{
	   	   
			DefaultMutableTreeNode devBranch = new DefaultMutableTreeNode(dev);
			
			LinkedList<EntityParam> props = fEntityParams.get(dev);
			for (EntityParam ep: props)
			{
				DefaultMutableTreeNode propBranch = new DefaultMutableTreeNode(ep);
				
				for (Entry<String, String> fieldent: ep.getFields().entrySet())
				{
					if (isStdField(fieldent.getKey())) continue;
					
					DefaultMutableTreeNode fieldNode = new DefaultMutableTreeNode(new Field(fieldent.getKey(),fieldent.getValue()));
					propBranch.add(fieldNode);
					//System.out.println("FlatTreeCreator::create "+ep.getDeviceName()+"/"+ep.getParameterName()+"#"+fieldent.getKey());
				}
				
				devBranch.add(propBranch);
			}
			
			
			root.add(devBranch);
		}


		
		return createTreeInPanel(root);
		
	}

}
