package cern.accsoft.op.referencesaver.gui.treeobjects;

public class Field {
	String fName;
	
	/**
	 * the string returned by SimpleParameterValue
	 */
	String fTypeString;
	
	public Field(String fieldName, String typeDescr)
	{
		fName = fieldName;
		fTypeString = typeDescr;
	}
	
	public String getName()
	{
		return fName; 
	}
	public String getTypeString()
	{
		return fTypeString; 
	}
	public String toString()
	{
		return fName+" : "+fTypeString; 
	}
	

}
