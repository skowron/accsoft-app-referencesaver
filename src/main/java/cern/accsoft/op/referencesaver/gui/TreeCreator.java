package cern.accsoft.op.referencesaver.gui;

import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.ToolTipManager;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cern.accsoft.op.referencesaver.gui.treeobjects.DeviceGroup;
import cern.accsoft.op.referencesaver.gui.treeobjects.EntityParam;
import cern.accsoft.op.referencesaver.gui.treeobjects.EntityParam.Status;
import cern.accsoft.op.referencesaver.gui.treeobjects.Field;
import cern.accsoft.op.referencesaver.gui.treeobjects.GroupEntityParam;
import cern.accsoft.op.referencesaver.nxcals.Reference;
import cern.japc.value.MapParameterValue;
import cern.japc.value.SimpleParameterValue;

public abstract class TreeCreator implements TreeSelectionListener {

	protected final Logger LOGGER = LoggerFactory.getLogger(getClass());
	
	ReferenceViewPanel fRefViewer;
	JTree  fTree;

	
	public TreeCreator(ReferenceViewPanel refViewer)
	{
		fRefViewer= refViewer;
	}
	
	public JTree getTree()
	{
		return fTree;
	}
	
	protected JComponent createTreeInPanel(DefaultMutableTreeNode root)
	{
		fTree = new JTree(root);
		fTree.setCellRenderer(new CustomTreeRenderer());
		fTree.addTreeSelectionListener(this);
		fTree.setVisibleRowCount(30);
		ToolTipManager.sharedInstance().setInitialDelay(200);
		ToolTipManager.sharedInstance().setDismissDelay(200);

		MouseAdapter ml = new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				mouseInTreePressed(e);
			}
		};
		fTree.addMouseListener(ml);


		JScrollPane panel = new JScrollPane(fTree,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,  JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		panel.setPreferredSize(new Dimension(500,500));
	    
		return panel;
	}
	
	//_________________________________________________________________
	protected int showParameterFieldsInTree(DefaultMutableTreeNode node, GroupEntityParam gep)
	{
		List<String> devNames = gep.getDeviceGroup().getDeviceNames();
		System.out.println("showParameterFieldsInTree: group "+ gep.getDeviceGroup().getName()+ " : "+ gep.getDeviceGroup().getDeviceNames());
		
		
		EntityParam ep = gep.getEntities().get(0);
		if (ep == null)
		{
			System.err.println("showParameterFieldsInTree: group "+ gep.getDeviceGroup().getName()+ " returned null entity");
			return 1; 
		}
		return showParameterFieldsInTree(node, ep);
		
		
	}

	
	protected int showParameterFieldsInTree(DefaultMutableTreeNode node, EntityParam ep)
	{
		
	    ep.setStatus(Status.RETRIEVING);
	    SwingUtilities.invokeLater( () -> reloadTree(node) );

	    List<MapParameterValue> values = fRefViewer.getReferences().get(0).getValues(ep.getEntity());
        System.out.println("ReferenceViewPanel::showParameter got "+values.size()+" values" );
        
		if (values.isEmpty())
	        {
	     	   DefaultMutableTreeNode emptyLeaf = new DefaultMutableTreeNode("<EMPTY>");
				   node.add(emptyLeaf); 
				   ep.setStatus(Status.ERROR);
				   SwingUtilities.invokeLater( () -> reloadTree(node) );
				   return 0 ;
	        }
	        
	        if (values.size()>1)
	        {
	     	   DefaultMutableTreeNode emptyLeaf = new DefaultMutableTreeNode("<MORE THAN ONE: "+values.size()+">");
			   node.add(emptyLeaf);
			   ep.setStatus(Status.NON_SUPPORTED);
			   SwingUtilities.invokeLater( () -> reloadTree(node) );
			   return 0;
			   
	        }
	        
	        MapParameterValue mpv =  values.get(0);
	        for (String field: mpv.getNames())
	        {
	     	   if (isStdField(field)) continue;
	     	   SimpleParameterValue spv = mpv.get(field);
	     	   
	     	   DefaultMutableTreeNode propBranch = new DefaultMutableTreeNode(new Field(field,spv.getValueType().toString()));
			   node.add(propBranch);
	        }
	        
	        ep.setStatus(Status.RETRIEVED);
	        SwingUtilities.invokeLater( () -> reloadTree(node) );
	        System.out.println("showParameterFieldsInTree: Done with "+node.toString()+ " Found "+mpv.size()+ " fields");
	        return mpv.size();
	}
	
	
	public void reloadTree(DefaultMutableTreeNode node2)
	{
		DefaultTreeModel model = (DefaultTreeModel)fTree.getModel();
		DefaultMutableTreeNode root = (DefaultMutableTreeNode)model.getRoot();
		model.reload(node2);
	}

    //_________________________________________________________________
	public void mouseInTreePressed(MouseEvent e) {
        int selRow = fTree.getRowForLocation(e.getX(), e.getY());
        TreePath selPath = fTree.getPathForLocation(e.getX(), e.getY());
        if(selRow != -1) 
         {
        	
           if( SwingUtilities.isRightMouseButton(e) )
           {
        	   JPopupMenu popup = new JPopupMenu();
        	   JMenuItem m1 = new JMenuItem("Show chi2 history");
        	   m1.addActionListener(l -> {showChi2history(selPath);});
        	   popup.add(m1);
        	   JMenuItem m2 = new JMenuItem("Show all details");
        	   m2.addActionListener(l -> {fRefViewer.showRefDetailPanel(selPath);});
               popup.add(m2);
            		   
               popup.show(fTree, e.getX(), e.getY());
               
           }
        		
            if(e.getClickCount() == 1) {
            	System.out.println("Tree clicked ones");
            }
            else if(e.getClickCount() == 2) {
            	System.out.println("Tree clicked twice");
            }
        }
    }	
	

	//_________________________________________________________________
	public void showParameter(TreePath tpath)
	{
		 if (tpath == null)
	     {
	         LOGGER.info("tree: selected path is NULL");
	         return;
	     }
	     
	     System.out.println("tree: selected path: {} " + tpath);
	     System.out.println("tree: tpath.getPathCount() =  {} " +tpath.getPathCount());
	     
	     if (tpath.getPathCount() < 1)
	       {
	           return;
	       }
	           
	    DefaultMutableTreeNode node0 = (DefaultMutableTreeNode) tpath.getPathComponent(0);
	      
	    DefaultMutableTreeNode node = (DefaultMutableTreeNode) tpath.getLastPathComponent();
	    Object uo = node.getUserObject();
	    System.out.println("tree: User object: "+uo.getClass().getSimpleName()+" "+ uo.toString());
	    
	    if (uo instanceof GroupEntityParam)
	    {
    		if (node.getChildCount() == 0)
    		{
    			CompletableFuture.supplyAsync(() -> showParameterFieldsInTree( node, (GroupEntityParam) uo));
 	    	   	return;
    		}
    		else
    		{
    			System.out.println("tree: GroupEntityParam, already resolved, nothing to do ");
    			return;
    		}
	    }
	    
	    if (uo instanceof EntityParam)
	     {
	       if (node.getChildCount() == 0)
	        { 
	    	   CompletableFuture.supplyAsync(() -> showParameterFieldsInTree(node, (EntityParam) uo) );
	    	   return;
	        }
	     }

	    if (uo instanceof Field)
	     {
	       
	       System.out.println("Clicked on Field "+uo+"  child count "+node.getChildCount());
	       
	       if (node.getChildCount() == 0)
	        { 
	    	   
		       Field field = (Field)uo;
		    	
		       DefaultMutableTreeNode papaNode = (DefaultMutableTreeNode)node.getParent();
		       Object papaUO = papaNode.getUserObject();
		       
		       if (papaUO instanceof EntityParam)
		       {// 1 device  
	    	     CompletableFuture.supplyAsync(() -> fRefViewer.showValueAndItsReference( (EntityParam)papaUO, field.getName()));
		       }
		       if (papaUO instanceof GroupEntityParam)
		       {// 1 device  
	    	     CompletableFuture.supplyAsync(() -> fRefViewer.showValuesAndTheirReferences( (GroupEntityParam)papaUO, field.getName()));
		       }
		       
	    	   return;
	        }
	       
	       
	     }
	    
	    	
	}	
    //_________________________________________________________________
	public static boolean isStdField(String field)
	{
		final List<String> nxcalsFields = Arrays.asList("acqStamp",
												       "cyclestamp",
												       "property",
												       "__record_version__",
												       "__record_timestamp__",
												       "__sys_nxcals_entity_id__",
												       "__sys_nxcals_partition_id__",
												       "__sys_nxcals_schema_id__",
												       "__sys_nxcals_system_id__",
												       "__sys_nxcals_timestamp__",
												       "selector", 
												       "nxcals_entity_id",
												       "class",
												       "device");

		return nxcalsFields.contains(field);
		
	}

    //_________________________________________________________________
	
	public void showChi2history(TreePath selPath)
	{
		
	}

    //_________________________________________________________________
	@Override
	public void valueChanged(TreeSelectionEvent e) 
	 {
	   try
		{
		 showParameter(e.getPath());
		}
	   catch(Exception ex)
		{
			LOGGER.error("valueChanged: handling tree action threw exception",ex);
		}
	}
	
	
	abstract public JComponent create();
	
	
}
