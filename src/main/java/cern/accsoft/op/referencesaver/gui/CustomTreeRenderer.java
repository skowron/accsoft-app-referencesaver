package cern.accsoft.op.referencesaver.gui;

import java.awt.Component;

import javax.swing.Icon;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;

import cern.accsoft.op.referencesaver.gui.treeobjects.EntityParam;

public class CustomTreeRenderer extends DefaultTreeCellRenderer {

    /**
     *
     */
    private static final long serialVersionUID = 967937360839211309L;

    public CustomTreeRenderer() {
 	    
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value,
            boolean sel, boolean expanded, boolean leaf, int row,
            boolean hasFocus) {
    	
    	//System.out.println("CustomTreeRenderer::getTreeCellRendererComponent: ");
    	
        super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);

        DefaultMutableTreeNode nodeObj = ((DefaultMutableTreeNode) value);
        
        
        if ( !(nodeObj.getUserObject() instanceof EntityParam) )
        {	
        	//System.out.println("CustomTreeRenderer::getTreeCellRendererComponent: Not an EntityParam ");
        	return this;
        }
        
        
        //System.out.println("CustomTreeRenderer::getTreeCellRendererComponent: This is an EntityParam ");
        
        try
        {
	        EntityParam ep = (EntityParam) nodeObj.getUserObject();
	        Icon icon = ep.getStatus().getIcon();
	        
	        //System.out.println("CustomTreeRenderer::getTreeCellRendererComponent: "+ ep.getParameterName()+" "+icon);
	        setIcon(icon);
	        
	        setBackground(ep.getType().getBgColor());
	        
	        
	        
        }
        catch(Exception ex)
        {
        	//System.err.println("CustomTreeRenderer::getTreeCellRendererComponent: EXCEPTION "+ ex.getMessage());
        	ex.printStackTrace();
        }
        
        return this;
    }
}
