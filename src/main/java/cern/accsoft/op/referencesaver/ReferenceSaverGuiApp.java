/**
 * Copyright (c) 2022 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.accsoft.op.referencesaver;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.TreeMap;

import javax.swing.JScrollPane;

import org.apache.spark.sql.SparkSession;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Import;

import cern.accsoft.ccs.ccda.client.core.CcdaClient;
import cern.accsoft.ccs.ccda.client.core.Environment;
import cern.accsoft.ccs.ccda.client.model.accelerator.Accelerator;
import cern.accsoft.commons.ccs.CcdaUtils;
import cern.accsoft.gui.beans.spi.SplashScreen;
import cern.accsoft.op.referencesaver.co.DataStore;
import cern.accsoft.op.referencesaver.gui.MainPanel;
import cern.accsoft.op.referencesaver.nxcals.Extractor;
import cern.accsoft.op.referencesaver.nxcals.Tagging;
import cern.accsoft.op.referencesaver.nxcals.TrimHistory;
import cern.japc.context.Context;
import cern.japc.gui.ascbeans.support.AppUtilities;
import cern.japc.gui.ascbeans.support.AppUtilities.AppArgs;
import cern.japc.gui.ascbeans.support.AppUtilities.AppArgsRequest;
import cern.japc.gui.ascbeans.support.ContextFrame;
import cern.japc.gui.ascbeans.support.ContextFrameBuilder;
import cern.japc.value.MapParameterValue;
import cern.nxcals.api.config.SparkContext;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.common.Constants;
import cern.rbac.common.RbaToken;
import cern.rbac.common.authentication.LoginPolicy;
import cern.rbac.util.authentication.LoginServiceBuilder;
import cern.rbac.util.lookup.RbaTokenLookup;

@Import(SparkContext.class)
@SpringBootApplication
public class ReferenceSaverGuiApp {
    protected static final Logger LOGGER = LoggerFactory.getLogger(ReferenceSaverGuiApp.class);
    
    private MainPanel fMainPanel;
    
    private static ReferenceSaverGuiApp fInstance;
    
    private DataStore             fDataStore;
    private SparkSession       fSparkSession = null;
    private Tagging          fTaggingService;
    private TrimHistory  fTrimHistoryService;
    private Extractor      fExtractorService;
    
    private Accelerator         fAccelerator;
     
    public static ReferenceSaverGuiApp getInstance()
    {
        if (fInstance == null)
        {
            fInstance =  new ReferenceSaverGuiApp();
        }
        return fInstance;
    }
    
    static {
        
        System.setProperty("service.url","https://cs-ccr-nxcals6.cern.ch:19093,https://cs-ccr-nxcals7.cern.ch:19093,https://cs-ccr-nxcals8.cern.ch:19093");
        System.setProperty("NXCALS_RBAC_AUTH", "true");
        System.setProperty(CcdaClient.CCDA_ENV, Environment.PRO.name());
        System.setProperty(Constants.KERBEROS_AUTH_DISABLE, "true");
        System.setProperty("CCM_NR", "1");
        System.setProperty("OPCONFIG", "LN4OP");
        System.setProperty("app.name", "ReferenceSaver");
        System.setProperty("app.version", "0.1");
        System.setProperty("Accelerator", "LN4");
        
    }
    

    @NotNull
    private static SparkSession createSparkSession(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(ReferenceSaverCmdApp.class, args);
        SparkSession  sparkSession = context.getBean(SparkSession.class);
        LOGGER.info("Got spark session {}", sparkSession);
        return sparkSession;
    }
    
    protected void setSparkSession(SparkSession sses, Accelerator thisAccelerator)
    {
    	fSparkSession = sses;
    	fAccelerator = thisAccelerator;
    	
    	fTaggingService  = new Tagging(fSparkSession, thisAccelerator);
    	fTrimHistoryService = new TrimHistory(fSparkSession);
    	fExtractorService = new Extractor(fSparkSession);
    	
    	
    }

    protected SparkSession getSparkSession()
    {
    	return fSparkSession;
    }
    
    /**
     * The default constructor will do all the GUI building.
     * 
     * @param appArgs Application arguments
     * @throws InterruptedException 
     */
    public ReferenceSaverGuiApp()
    {
    	fDataStore = new DataStore();
    }
    
    public Accelerator getAccelerator()
    {
    	return fAccelerator;
    }

    
    public void startMonitoring(Context ctxIn) 
    {
    	fDataStore.startMonitoring(ctxIn);
    }
    
    public MainPanel getMainPanel()
    {
         if (fMainPanel == null)
        {
            fMainPanel = new MainPanel(); 
         }
        
        return fMainPanel;
    }

    public DataStore getDataStore()
    {
    	return fDataStore;
    }
    
    public Tagging      getTaggingService()
    {
    	return fTaggingService;
    }
    public TrimHistory  getTrimHistoryService()
    {
    	return fTrimHistoryService;
    }

    public Extractor  getExtractorService()
    {
    	return fExtractorService;
    }

    
    private boolean test()
    {
    	boolean exitAfterTest = true;
    	
    	
    	Extractor extractor = getInstance().getExtractorService();
		Instant now = Instant.now();
		Instant tFrom = now.minus(Duration.ofDays(1));
		TimeWindow twin = TimeWindow.between(tFrom,now);
		System.out.println("ReferenceViewPanel::showValue: Extracting data from NXCALS from "+ tFrom + " to "+ now);
		
//    	Dataset<Row> funds = extractor.findLsaCycleFundamentals(twin, "TOF_2024");
//    	funds.printSchema();
//    	long nfunds = funds.count();
//    	System.out.println("Found "+nfunds+" fundamentals");

    	String field = "currentLinacSingle";
    	
    	TreeMap<Date, MapParameterValue> values = null;
    	try
    	{
    		//TreeMap<Date, MapParameterValue> values = extractor.getValues("L4L.BCT.3113","Acquisition","TOF_2024",twin);
    	  //values = extractor.getValues("L4L.AC.22","STATE","TOF_2024",twin);
    		values = extractor.getValues("L4L.AC.22","MEAS.V.VALUE","TOF_2024",twin);
    	}
    	catch (Throwable ex)
    	{
    		System.err.println("Exception with getValues");
    		ex.printStackTrace();
    		return true;
    	}
    	
    	
    	System.out.println("Found "+values.size()+" values");
    	
		
		//mpvs = extr.getValues(ep.getDeviceName(), ep.getPropertyName(), fLsaCycleName, );
    	
    	return exitAfterTest;
    }
    
    
    /**
     * @param args
     */
    public static void main(final String[] args) {
        // You can use your own picture here
        
      
    	LoginServiceBuilder builder = LoginServiceBuilder.newInstance();
    	builder.loginPolicy(LoginPolicy.KERBEROS);
    	builder.applicationName("ReferenceSaver");
    	builder.build();
    	RbaToken token = RbaTokenLookup.findClientTierRbaToken();
    	System.out.println("Current token: " + token);
    	
    	
    	String thisAcceleratorName = System.getProperty("Accelerator");
    	

        Accelerator thisAccelerator = CcdaUtils.getAccelerator(thisAcceleratorName);
        if (thisAccelerator== null)
        {
        	System.err.println("Cannot accelerator you want to work with. Please pass valid name via property \"Accelerator\" ");
        	System.err.println("Property \"Accelerator\" is "+thisAcceleratorName);
        	System.exit(0);
        }
        
        
        ReferenceSaverGuiApp theObj = ReferenceSaverGuiApp.getInstance();
        
        SparkSession sparkSession = createSparkSession(args);
        if (sparkSession == null)
        {
        	LOGGER.error("Could not create Spark session. Exiting.");
        	return;
        }
        theObj.setSparkSession(sparkSession, thisAccelerator);

        
        
//        if (theObj.test()) 
//        {
//        	System.err.println("Exiting after test execution");
//        	System.exit(1);
//        }
        

    	
        final SplashScreen splashScreen = new SplashScreen(null, Toolkit.getDefaultToolkit().getImage(
                ReferenceSaverGuiApp.class.getResource("/Airport_reference_point.jpg")), Color.BLUE.darker());
        
        
        try {
            // this call is MANDATORY as it initialises all the variables needed by your applications
            AppUtilities.initApp();

            // this code is OPTIONAL it allow you to retrieve arguments values
            // you may need to initialise your application
            AppArgsRequest appArgsRequest = new AppArgsRequest(args);
            
            
            // the pls equation is mandatory here
            appArgsRequest.setPlsConditionMandatory(true);
            // parameter names are optionnal
            appArgsRequest.setParameterNamesRequested(true);
            AppArgs appArgs = AppUtilities.getAppArgs(appArgsRequest, splashScreen);
            
            if (appArgs.getOtherArgs().contains("DEV"))
            {
            	Config.setDev(true);
            	System.out.println("Using DEVELOPMENT node");
            }
            else
            {
                System.out.println("Using PRODUCTION node");
            }
            
            
            String plsc = appArgs.getPlsCondition();
            System.out.println("PLS Condition: <"+plsc+">");
            
            //override it
            
            plsc = Config.getPlsAll();
            
            // this call is MANDATORY it opens your panel in a ContextFrame window
            
            
            
            
            //theObj.getMainPanel().updateUserStatusesPane();
            
         
            ContextFrameBuilder framebuilder = ContextFrameBuilder.newInstance()
            .setParentFrame(null)
            .setRootComponent(new JScrollPane(theObj.getMainPanel())) // your panel)
            .setUseAscToolbar(true)
            .setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE)
            .setSplashScreen(splashScreen)
            .setPls(plsc)
            .setDimension(new Dimension(1600,1000))
            .setTitle("References")
            .setMonitoring(true);
           
            
            
            System.out.println("Starting ContextFrameBuilder ....");
            
            ContextFrame ctxFrame = framebuilder.build();

            System.out.println("Starting ContextFrameBuilder .... Done");
            
            
            theObj.startMonitoring( ctxFrame.getContext() );
            
            
            
            
            
        } catch (Exception ex) {
            AppUtilities.dumpError(ReferenceSaverGuiApp.class.getSimpleName(), ex);
        }
    }

    

}