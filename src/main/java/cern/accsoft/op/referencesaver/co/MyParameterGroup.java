package cern.accsoft.op.referencesaver.co;

import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import cern.accsoft.op.referencesaver.Config;
import cern.cmw.data.Data;
import cern.cmw.data.DataFactory;
import cern.cmw.rda3.client.core.AccessPoint;
import cern.cmw.rda3.client.service.ClientService;
import cern.cmw.rda3.client.subscription.NotificationListener;
import cern.cmw.rda3.client.subscription.Subscription;
import cern.cmw.rda3.common.data.AcquiredData;
import cern.cmw.rda3.common.data.RequestContextFactory;
import cern.cmw.rda3.common.data.UpdateType;
import cern.cmw.rda3.common.exception.NameServiceException;
import cern.cmw.rda3.common.exception.RdaException;
import cern.japc.core.AcquiredParameterValue;
import cern.japc.core.FailSafeParameterValue;
import cern.japc.core.Parameter;
import cern.japc.core.Selector;
import cern.japc.core.ValueHeader;
import cern.japc.core.factory.FailSafeParameterValueFactory;
import cern.japc.core.factory.SelectorFactory;
import cern.japc.core.factory.ValueHeaderFactory;
import cern.japc.core.group.FailSafeParameterValueListener;
import cern.japc.ext.cmwrda3.DataTranslator;



public class MyParameterGroup implements NotificationListener{
	 ClientService client = null;
	 private final BiMap<Parameter, Subscription> parameterList = HashBiMap.create();
	 private boolean fIsMonitoring = false;
	 DataStore fDataStore = null;
	 Selector fSelector = null;
	 
	 
	 public MyParameterGroup(DataStore dataStore, Selector selector) throws RdaException
	 {
		 fDataStore =  dataStore;
		 client = ClientService.create();
		 fSelector = selector;
		 
		 
	 }
	 //________________________________________________________________
	 public void addAll(Parameter[] parameters) {
		 for (Parameter parameter: parameters)
		  {
	         System.out.println("MyParameterGroup:: addAll: adding "+ parameter); 
			 parameterList.put(parameter, null);
		  }
	    }
	//_________________________________________________________________________________________
	 
	 public boolean isMonitoring()
	 {
		 return fIsMonitoring;
	 }
	 //_________________________________________________________________________________________
	 
	 public void createSubscription(Selector selector, FailSafeParameterValueListener listener) 
	 {
		 System.out.println("++++++++++++++++++++++++++++++++++++++++++++++");
		 System.out.println("MyParameterGroup::createSubscriptions: ");
		 
		 List<Parameter> parsFailed = new LinkedList<>();
		 
		 for (Entry<Parameter, Subscription> entry: parameterList.entrySet())
		 {
			 Parameter parameter = entry.getKey();
			 
			 Subscription sub = null;
			 try {
				 System.out.println("MyParameterGroup::createSubscriptions: "+ parameter.getDeviceName() +" / "+ parameter.getPropertyName());
				 
				 AccessPoint ap = client.getAccessPoint(parameter.getDeviceName(), parameter.getPropertyName());
				 sub = ap.subscribe(RequestContextFactory.create(Config.getPlsAll()), this);

			 } catch (Throwable e) {
				 e.printStackTrace();
				 
				 parsFailed.add(parameter);
				 continue;
			 }
			 
			 entry.setValue(sub);
		 }
		 
		 fIsMonitoring = true;

		 System.out.println("MyParameterGroup::createSubscriptions: Failed to start "+parsFailed.size());
		 
		 for (Parameter parameter: parsFailed)
		 {
			 parameterList.remove(parameter);
		 }
		 
		 
	 }
	//_________________________________________________________________________________________
	 
	 public void stopMonitoring()
	 {
		 fIsMonitoring = false;
		 
		 for (Entry<Parameter, Subscription> entry: parameterList.entrySet())
		 {
			 Subscription sub = entry.getValue();
			 sub.unsubscribe();
			 entry.setValue(null);
		 }
		 parameterList.clear();
		 
	 }
//_______________________________________________________

	 @Override
	 public void dataReceived(Subscription subscription, AcquiredData acqData, UpdateType updateType) {
		 if(updateType == UpdateType.UT_FIRST_UPDATE)
		 {
			 return;
		 }
		 
		 Data parameter_value = acqData.getData();
		 String devname = subscription.getAccesPoint().getDeviceName();
		 String  propname = subscription.getAccesPoint().getPropertyName();
		 
		 
		 Selector thisCycleSelecta = SelectorFactory.newSelector( acqData.getContext().getCycleName());
		 
		 ValueHeader header = ValueHeaderFactory.newAcquisitionRegularUpdateHeader(acqData.getContext().getAcqStamp(),
				 acqData.getContext().getCycleStamp(), 
				 thisCycleSelecta);
		 Data dataHeader = DataTranslator.createDataForValueHeader(header);
		 
		 Data data = DataFactory.createData();
		 
		 data.append("header", dataHeader);
		 data.append("parameter_value", parameter_value);
		 data.append("parameter_name",  devname +"/"+propname);
		 
		 
		 FailSafeParameterValue fspv = DataTranslator.createFailSafeParameterValue(data);
		 
//		 Parameter param = parameterList.inverse().get(subscription);
//		 AcquiredParameterValue val = DataTranslator.createParameterValue(acqData, fSelector, param, updateType );
//		 //DataTranslator.createValueHeader(acqData.getContext(), fSelector, updateType);
		 
				 
//				 
//		 FailSafeParameterValue value = FailSafeParameterValueFactory.newFailSafeParameterValue(val);
		 
		 fDataStore.valueReceived(fspv);

	 }
	//_________________________________________________________________________________________
	 @Override
	 public void errorReceived(Subscription subscription, RdaException exception, UpdateType updateType) {
		 // TODO Auto-generated method stub
		 System.out.println(String.format("%s\n%s\n", "Error received: ", exception.getMessage()));
	 }
	 
	 
}
