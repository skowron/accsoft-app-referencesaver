package cern.accsoft.op.referencesaver.co;

import static cern.lsa.domain.settings.ContextSettingsRequest.byStandAloneContextAndParameters;
import static cern.lsa.domain.settings.TrimHeadersRequest.byBeamProcessesAndParameters;
import static java.util.Collections.singleton;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cern.accsoft.commons.value.ValueFactory;
import cern.accsoft.gui.frame.MessageManager;
import cern.accsoft.op.referencesaver.Config;
import cern.japc.core.factory.ParameterFactory;
import cern.lsa.client.ContextService;
import cern.lsa.client.ParameterService;
import cern.lsa.client.ServiceLocator;
import cern.lsa.client.SettingService;
import cern.lsa.client.TrimService;
import cern.lsa.domain.exploitation.DriveException;
import cern.lsa.domain.settings.ContextSettings;
import cern.lsa.domain.settings.CopySettingsRequest;
import cern.lsa.domain.settings.Parameter;
import cern.lsa.domain.settings.ParameterSettings;
import cern.lsa.domain.settings.Setting;
import cern.lsa.domain.settings.SettingsSource;
import cern.lsa.domain.settings.StandAloneCycle;
import cern.lsa.domain.settings.TrimException;
import cern.lsa.domain.settings.TrimHeader;
import cern.lsa.domain.settings.TrimRequest;
import cern.lsa.domain.settings.TrimResponse;
import cern.lsa.domain.settings.factory.TrimRequestBuilder;

public class LsaUtils {

	protected static final Logger LOGGER = LoggerFactory.getLogger(LsaUtils.class);


	public static List<TrimHeader> findTrims( String paramNane, String cycleName) {
		ParameterService parameterService = ServiceLocator.getService(ParameterService.class);
		ContextService     contextService = ServiceLocator.getService(ContextService.class);
		TrimService           trimService = ServiceLocator.getService(TrimService.class);

		// Find parameter(s) and context
		Parameter parameter = parameterService.findParameterByName(paramNane);
		StandAloneCycle cycle = contextService.findStandAloneCycle(cycleName);


		// Find all trim entries on given parameter(s) - the list is sorted by trim date
		List<TrimHeader> trims = trimService.findTrimHeaders(
				byBeamProcessesAndParameters(cycle.getBeamProcesses(), singleton(parameter)));

		// The last trim
		TrimHeader lastTrim = trims.get(trims.size() - 1);
		System.out.println("Last Trim: " + lastTrim);


		return trims;
	}
 
	public static void trimSetting(String parname, String lsacyclename, Object newref) 
	{

		ParameterService parameterService = ServiceLocator.getService(ParameterService.class);
		ContextService contextService = ServiceLocator.getService(ContextService.class);
		TrimService trimService = ServiceLocator.getService(TrimService.class);

		// Find parameter(s) and context
		Parameter parameter = parameterService.findParameterByName(parname);
		StandAloneCycle cycle = contextService.findStandAloneCycle(lsacyclename);
		if (cycle == null)
		{
			MessageManager.error("Exception while setting new reference in LSA parameter "+parname,
					new Exception("Cannot find cycle ["+lsacyclename+"]"),
					null);
			System.err.println("LsaUtils::trimSetting: Cannot get cycle "+ lsacyclename);
			return;
			
		}
		String systemName;
		try {
			systemName = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			systemName = "<unknown computer name>";
			
		}
		// trim
		TrimRequest trimRequest = new TrimRequestBuilder() //
				.setDescription("Reference Saver application on "+systemName) //
				.setContext(cycle) //
				.addScalar(parameter, ValueFactory.createScalar(newref)) //
				// .setTransient(true) // to mark trim as transient
				// .setDrive(false) // to not drive (relevant if context is resident)
				// .setPersistSettings(false) // do not persist settings to LSA
				.build();

		TrimResponse trimResponse = null;
		try {
			trimResponse = trimService.trimSettings(trimRequest);
		} catch (Exception e) {
			MessageManager.error("Exception while setting new reference in LSA parameter "+parname,e,null);
			System.err.println("LsaUtils::trimSetting: Exception while setting new reference in LSA parameter");
			e.printStackTrace();
		} 
		System.out.println("LsaUtils::trimSetting: "+trimResponse);

	}
	public static Set<Setting> findTrimValues(String parname,  String cycleName, List<TrimHeader> trims) {

		LOGGER.info("Looking for settings of {} in {} ", parname, cycleName);
		// Find parameter(s) and context
		ParameterService parameterService = ServiceLocator.getService(ParameterService.class);
		ContextService     contextService = ServiceLocator.getService(ContextService.class);


		Parameter parameter = parameterService.findParameterByName(parname);

		if (parameter == null)
		{
			LOGGER.error("There is no parameter {} for this accelerator", parname);
			return new HashSet<Setting>();
		}
		StandAloneCycle cycle = contextService.findStandAloneCycle(cycleName);

		LOGGER.info("Parameter {}", parameter);
		LOGGER.info("Cycle {}", cycle);

		// Find date of the last trim
		SettingService settingService = ServiceLocator.getService(SettingService.class);
		ContextSettings contextSettings = settingService
				.findContextSettings(byStandAloneContextAndParameters(cycle, singleton(parameter)));

		LOGGER.info("contextSettings {}", contextSettings);

		ParameterSettings parameterSettings = contextSettings.getParameterSettings(parameter);

		LOGGER.info("parameterSettings {}", parameterSettings);

		Set<Setting> settings = parameterSettings.getSettings();

		return settings;

	}



}
