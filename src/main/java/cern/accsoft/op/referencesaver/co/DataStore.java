package cern.accsoft.op.referencesaver.co;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cern.accsoft.gui.frame.MessageManager;
import cern.accsoft.op.referencesaver.Config;
import cern.accsoft.op.referencesaver.ReferenceSaverGuiApp;
import cern.accsoft.op.referencesaver.gui.MainPanel;
import cern.accsoft.op.referencesaver.gui.treeobjects.EntityParam;
import cern.accsoft.op.referencesaver.plotters.Plotter;
import cern.cmw.rda3.common.exception.RdaException;
import cern.japc.context.Context;
import cern.japc.context.ContextAware;
import cern.japc.context.CtxBeamEvent;
import cern.japc.context.CtxBeamListener;
import cern.japc.context.CtxConstants;
import cern.japc.context.CtxDataEvent;
import cern.japc.context.CtxDataListener;
import cern.japc.context.CtxTriggerEvent;
import cern.japc.core.FailSafeParameterValue;
import cern.japc.core.Parameter;
import cern.japc.core.ParameterException;
import cern.japc.core.factory.ParameterFactory;
import cern.japc.core.factory.SelectorFactory;
import cern.japc.core.group.FailSafeParameterValueListener;
import cern.japc.core.spi.ParameterUrlImpl;
import cern.japc.gui.viewer.support.ValueChangedEvent;
import cern.japc.gui.viewer.support.ValueChangedListener;
import cern.japc.value.MapParameterValue;


public class DataStore implements Runnable, ValueChangedListener, CtxBeamListener, CtxDataListener, ContextAware, FailSafeParameterValueListener  {
/**
 * It is intended to operate with USER.ALL, and it will push data to registered Listeners only when user matches
 * 
 * We cannot use Context for all subscriptions because it fails if property has only some fields imported to InCA.
 * So for data plotting we go directly RDA. To be seen if it works for all devices that we need in this application.  
 */
    protected static final Logger LOGGER = LoggerFactory.getLogger(DataStore.class);
    
    
    /**
     * Global parameters, always subscribed
     */
	private Map<String,String>   fParameterNames = new HashMap<String,String>();
	/**
	 * Parameters requested by Plotters
	 */
	
	private Map<String,List<Plotter>> fParameterNamesForPlotters = new HashMap<String, List<Plotter>>();
	private MyParameterGroup           fSubscriptionsForPlotters = null;
	
	private Context                     fContext = null;
    
	private Integer               fThreadCommand = 0;   
    private Thread         fDataProcessingThread;
    
    private List<FailSafeParameterValue>    fLastData = new ArrayList<>();;
    private TreeMap<Long, CtxTriggerEvent>  fEventMap = new TreeMap<Long, CtxTriggerEvent>(); 
    private CtxTriggerEvent fLastDataTriggerEvent ;
    private Object                fLastDataLock = new Object();
    private Integer             fDataProcessFlag = 0;
    private Object          fDataProcessFlagLock = 0;

    private Object                    fDataLock  = new Object();
    
    private List<FailSafeParameterValue>    fNonCycleBoundData = new ArrayList<>();
    
    //________________________________________________________________________________
    public DataStore() 
    {
        fParameterNames.put("XTIM.BX.SCY-CT/Acquisition","PSB.USER.ALL");
    }
    //________________________________________________________________________________
    
    public synchronized void restartSubscription()
    {
    	
    	
    	if (fSubscriptionsForPlotters != null && fSubscriptionsForPlotters.isMonitoring() )
        {	
        	System.out.println("DataStore::restartSubscription: stopping existing one ... ");
        	fSubscriptionsForPlotters.stopMonitoring();
        	System.out.println("DataStore::restartSubscription: stopping existing one ... Done");
        }
        
    	if (fSubscriptionsForPlotters == null) 
    	{
    		try {
    			// will need to handle null selectors later on
    			fSubscriptionsForPlotters = new MyParameterGroup(this,  SelectorFactory.newSelector(Config.getPlsAll()));
    		} catch (RdaException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    			//JOptionPane.showConfirmDialog(null, "Failed starting RDA subscription service, online data monitoring will not be possible");
    			MessageManager.warn("Failed starting RDA subscription service, online data monitoring will not be possible", e, null);
    			return;
    		}
    	}
        
    	fSubscriptionsForPlotters.addAll(createParameters(fParameterNamesForPlotters.keySet()));
        fSubscriptionsForPlotters.createSubscription(SelectorFactory.newSelector(Config.getPlsAll()), this);
        
    }
    //________________________________________________________________________________
    
    public void addParameter(String parameterName,  Plotter plotter)
    {
    	
      System.out.println("DataStore::addParameter: "+parameterName+ " for plotter "+plotter);
    	
      List<Plotter> plotters = fParameterNamesForPlotters.computeIfAbsent(parameterName,l -> new LinkedList<Plotter>());
      plotters.add(plotter);
      
      
      restartSubscription();
    	 
    }
    //________________________________________________________________________________

    public void removeParameter(String parameterName, Plotter plotter)
    {
    	
    	 List<Plotter> plotters = fParameterNamesForPlotters.get(parameterName);
    	 if (plotters == null)
    	 {
    		 return;
    	 }
    	 plotters.remove(plotter);
    	 
    	 if (plotters.isEmpty())
    	 {
    		 fParameterNamesForPlotters.remove(parameterName);
    	 }
    	 
    	 restartSubscription();
    	
    }
    
    @Override
    public void beamOccurred(CtxBeamEvent ctxBeamEvent) 
    {
    	String tgmCycle = ctxBeamEvent.getTriggerEvent().getCycleName();
    	System.out.println("DataStore::beamOccurred "+ ctxBeamEvent+" @ "+tgmCycle);
    	
        if (ctxBeamEvent.getType() != CtxBeamEvent.CTX_BEAM_END_DATA_OK)
        {
            System.out.println("DataStore::beamOccurred: we skip this beam event ");
        	return;
        }
        
        System.out.println("\n###############################################");
        System.out.println("NOW WILL PROCESS THE DATA");
        
        setDataProcessFlag(1);//please process new data
    }
    //________________________________________________________________________________

    
     public Parameter[] createParameters(Set<String> parnames)
     {
    	 Parameter[] params = new Parameter[parnames.size()];
    	 ParameterFactory pfactory = ParameterFactory.newInstance();
    	 
    	 int i = 0;
    	 for (String paramName: parnames)
    	 {
    		 try 
    		 {
    			 params[i++]  = pfactory.newParameter(paramName);
    		 }
    		 catch (ParameterException e) {

    			 JOptionPane.showConfirmDialog(null, "Failed to find paramter "+paramName);
    			 params[i++]  = null;
    		 }
    	 }
    	 return params;
     }
     //________________________________________________________________________________
          
	 public void startMonitoring(Context ctxIn) 
	    {
	        fContext = ctxIn;
	        
	        ctxIn.setTimeout( CtxConstants.timeoutWaitingForData );
	        
	        Parameter[] params = createParameters(fParameterNames.keySet());
	        ctxIn.registerParameters(params, this);
	        
	        
	        ctxIn.addBeamListener(this);
	        
	        fThreadCommand = 1;// keep running
	        fDataProcessingThread = new Thread(this);
	        fDataProcessingThread.start();
	        
	    }

	    @Override
	    public void valueChanged(ValueChangedEvent valueChangedEvent) 
	    {
	        LOGGER.info("valueChanged: Got update from {}",valueChangedEvent.getSource());
	    }   
	    
	    @Override
	    public Context getContext() {
	        return fContext;
	    }
	    @Override   
	    public void setContext(Context context) {
	       LOGGER.info("setContext {}", context.getName());
	       fContext = context;
	    }
	    @Override
	    public void run() {
	        
	        int flag = 1;
	        
	        while( flag  == 1)
	        {
	            //System.out.println("Ctx state: "+fContext.getState());
	            try
	            {
	              Thread.sleep(100);
	            }
	            catch(InterruptedException ex)
	            {
	                System.err.println("Got interrupted");
	                return;
	            }
	            
	            if (getDataProcessFlag() == 1)
	             {
	                synchronized (fLastDataLock)
	                {
	                  LOGGER.trace("Processing data ...");
	                  processLastData();
	                  LOGGER.trace("Processing data ... Done");
	                }
	             }
	            //else
	            //    LOGGER.info("Keep sleeping ...");
	            
	            flag = getThreadCommand();
	        }
	        LOGGER.info("Exiting run()");
	    }
	    protected synchronized Integer getThreadCommand()
	    {
	        return fThreadCommand;
	    }
	    //___________________________________________________________________________________    
	    private int getDataProcessFlag()
	    {
	      synchronized(fDataProcessFlagLock)
	        {
	            return fDataProcessFlag;
	        }
	    }
	    
	    private void setDataProcessFlag(int flag)
	    {
	        synchronized(fDataProcessFlagLock)
	         {
	            fDataProcessFlag = flag;
	         }
	    }

	    //_________________________________________________________________
	    private void processLastData()
	    {
	      
	      try { 
	           
	       synchronized (fDataLock)
	        {
	          processLastDataSynched();
	        }
	       LOGGER.trace("Calling Update Status pane ...");
	       
	       LOGGER.trace("Calling Update Status pane ... Done");
	      }
	      catch (Exception ex)
	      {
	         LOGGER.error("Exception in processLastData ",ex);
	      }
	      finally
	      {
	         setDataProcessFlag(0); // data was processed 
	         fLastData.clear();
	         System.out.println("DataStore::processLastData: data buffer cleared");
	         System.out.println("END OF DATA PROCESSING");
   		     System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
   		     System.out.println("NEW BUFFER");

	      }
	    }
	    //_______________________________________________________________
	    private void getNonCycleBoundValues()
	    {
	    	java.util.Iterator<FailSafeParameterValue> it = fNonCycleBoundData.iterator();
	    	long cStart = fLastDataTriggerEvent.getCycleStamp();
	    	long cEnd = cStart + fLastDataTriggerEvent.getCycleDuration()*1200000000;
	    	long nowTs = Instant.now().toEpochMilli()*1000000; 
	    	
	    	
	    	while(it.hasNext())
	    	{
	    		FailSafeParameterValue v = it.next();
	    		long aqnStamp = v.getHeader().getAcqStamp();
	    		
	    		if ((aqnStamp >= cStart) && (aqnStamp < cEnd ) )
	    		 {
                     fLastData.add(v);
                     it.remove();
	    		 }
	    		
	    		if (  (nowTs  - aqnStamp ) > 100*1000000000L)
	    		{
	    			// older than 100 secs
	    			it.remove();
	    		}
	    		
	    	}
	    }
	    //_______________________________________________________________
	    
	    private void processLastDataSynched()
	    {
	        /**
	         * Acquisition has these fields
	         */
	        
	        //rewrite the method: remove all the logic and just copy what UCAP dev reports
	        
	    	long ts = fLastDataTriggerEvent.getCycleStamp();
	    	
	    	MainPanel mainPanel = ReferenceSaverGuiApp.getInstance().getMainPanel();
	        String seluser = mainPanel.getSelectedTgmUser();
	        String triguser = fLastDataTriggerEvent.getFullCycleName();
	        
	        // it is for the list of last cycle stamps to reference saving
	        if (seluser == null || seluser.isEmpty())
	        {
	        	mainPanel.setBeamLabel(triguser, ts);
	        }
	        else if (seluser.equals(triguser))
	        {
	        	mainPanel.setBeamLabel(triguser, ts);
	        }
	        

	        
	        
	        FailSafeParameterValue xtimstartCycle = getParameterValue("XTIM.BX.SCY-CT/Acquisition");
	        if (xtimstartCycle == null)
	        {
	        	MessageManager.error("Did not receive XTIM info", null, null);
	        	return;
	        }
	        
	        
	        if ( !xtimstartCycle.isValue())
	        {
	        	
	        	MessageManager.error("Received XTIM info is not a value", xtimstartCycle.getException(), null);
	        	return;
	        	
	        }
	        
	        MapParameterValue xtimmpv = xtimstartCycle.getValue().castAsMap();
	        
	        String xtimLsaCycleName = xtimmpv.getString("lsaCycleName");
	        
	        if (xtimLsaCycleName == null || xtimLsaCycleName.isEmpty())
	        {
	        	MessageManager.error("Received XTIM does not provide LSA cycle name", null, null);
	        	return;
	        }
	        
	        getNonCycleBoundValues();
	        
	        System.out.println("XTIM : lsaCycleName "+xtimLsaCycleName );
	        
	        for (String reqParam: fParameterNamesForPlotters.keySet())
	        {
	        	List<Plotter> plotters = fParameterNamesForPlotters.get(reqParam);
	        	
	        	for (Plotter plotter: plotters)
	        	{
	        	   if ( xtimLsaCycleName.equals(plotter.getLsaCycleName()) )
	        		{
	         		   FailSafeParameterValue fspv = getParameterValue(reqParam);
	        		   if (fspv == null) continue;
	        		   System.out.println("DataStore::processLastDataSynched: propagating to plotter "+reqParam+" @ "+xtimLsaCycleName );
	        		   plotter.valueReceived(fspv, ts);
	        		}
	        	}
	        	
	        }
	        
	        //LOGGER.trace("itlkstatusaqn {}", timinginfopv);
	    }
	    //___________________________________________________________________________________
	    private FailSafeParameterValue getParameterValue(String parameterName)
	    {
	        if (fLastData == null)
	        {
	            LOGGER.info("Last data is null");
	            return null;
	        }
	        for (FailSafeParameterValue fpv: fLastData)
	        {
	            if (! fpv.getParameterName().equalsIgnoreCase(parameterName))
	            {
	                continue;
	            }
	            System.out.println("DataStore::getParameterValue: returning "+ parameterName);
	            return fpv;
	        }
	        System.out.println("DataStore::getParameterValue: did not find "+ parameterName);
	        return null;
	    	
	    }
	    //___________________________________________________________________________________
	    private MapParameterValue getParameterValueAsMap(String parameterName)
	    {
	        if (fLastData == null)
	        {
	            LOGGER.info("Last data is null");
	            return null;
	        }
	        
	        for (FailSafeParameterValue fpv: fLastData)
	        {
	            if (! fpv.getParameterName().equalsIgnoreCase(parameterName))
	            {
	                continue;
	            }
	            if (fpv.getValue() == null)
	            {
	            	return null;
	            }
	            
	            return fpv.getValue().castAsMap();
	            
	        }
	        return null;
	    }
	    //___________________________________________________________________________________
	    /**
	     * Push data to proper plotter
	     * We need to find it by LSA cycle name
	     * if this information is not available then we push it to dedicated buffer and then we process it together with data received from Context 
	     * @param fspv
	     * @return
	     */
	    public int processValue(FailSafeParameterValue fspv)
	    {
	    	if (fspv == null)
	    	{
	    		return 1;
	    	}
	    	
	    	String tgmCycleName = fspv.getHeader().getSelector().getId();
	    	Map<String, String> cmap = ReferenceSaverGuiApp.getInstance().getMainPanel().getCycleMapping();
	    	String lsaCycleName = cmap.get(tgmCycleName);
	    	String parName = fspv.getParameterName();
	    	if (lsaCycleName == null || lsaCycleName.isEmpty())
	    	{
	    	  // if there is no cycle information we must resolve by AQN time
	    	  synchronized (fDataLock)
		        {
	    		   fNonCycleBoundData.add(fspv);
		        }
	    		return 0;
	    	}
	    	
	    	long ts = fspv.getHeader().getCycleStamp();
	    	if (ts < 1)
	    	{
	    		ts = fspv.getHeader().getAcqStamp();
	    	}
	    	
	    	System.out.println("DataStore::processValue "+parName+" @  "+ tgmCycleName + " : "+ lsaCycleName);
	    	List<Plotter> plotters = fParameterNamesForPlotters.get( parName );
	    	if (plotters == null)
	    	{
	    		System.out.println("DataStore::processValue: strange, no plotter registered for this parameter "+parName);
	    		return 2;
	    	}

	    	ParameterUrlImpl parurl = new ParameterUrlImpl(fspv.getParameterName());
	    	
	    	for (Plotter plotter: plotters)
	    	{
	    		//System.out.println("DataStore::processValue: plotter for cycle " +plotter.getLsaCycleName() + " ? ");
	    		if ( !lsaCycleName.equals(plotter.getLsaCycleName()) )
	    		{
	    			continue;
	    		}
	    		//System.out.println("DataStore::processValue: plotter for cycle " +plotter.getLsaCycleName() + " ?  YES !");
	    		
	    		plotter.valueReceived(fspv, ts);

	    		EntityParam ep = plotter.getReferenceViewPanel().getEntityParam(parurl.getDeviceName(), parurl.getPropertyName());
	    		if (ep != null && fspv.getValue() != null)
	    		{
	    			ep.addBufferedValue(Date.from(Instant.ofEpochSecond(0, ts)), fspv.getValue().castAsMap());
	    		}
	    	}
	      return 0;
	    }
	    
	    //__________________________________________________________________________________

		
        /**
         * Values from RDA or JAPC subscription for plotters
         * we do not buffer here in data store, but push directly to plotters and to buggers in the EntityParam in the JTrees
         * @param fspv
         */
		public void valueReceived(FailSafeParameterValue[] values) 
		{

	    	for (FailSafeParameterValue fspv : values)
	    	{
	    		try
	    		{
	    			processValue(fspv);
	    		}
	    		catch (Throwable e)
	    		{
	    			String parName = "<parameter is null>";
	    			if (fspv != null)
	    			{
	    				parName = fspv.getParameterName();
	    			}
	    			MessageManager.error("Exception whiole processing received parameter "+parName , e, null);
	    		}
	    	}			
		}
	    //___________________________________________________________________________________
        /**
         * Values from RDA or JAPC subscription for plotters
         * we do not buffer here in data store, but push directly to plotters and to buggers in the EntityParam in the JTrees
         * @param fspv
         */
		public void valueReceived(FailSafeParameterValue fspv) 
		{
    		try
    		{
    			processValue(fspv);
    		}
    		catch (Throwable e)
    		{
    			String parName = "<parameter is null>";
    			if (fspv != null)
    			{
    				parName = fspv.getParameterName();
    			}
    			MessageManager.error("Exception whiole processing received parameter "+parName , e, null);
    		}

		}
		
	    //___________________________________________________________________________________    
	    @Override
	    public void dataOccurred(CtxDataEvent dataEvent) {
	    	
	    	System.out.println("DataStore::dataOccurred @ "+ dataEvent.getTriggerEvent().getFullCycleName());
	    	
	        synchronized (fLastDataLock) {
	            fLastData.addAll( Arrays.asList(dataEvent.getParameterValueArray()));
	            fLastDataTriggerEvent = dataEvent.getTriggerEvent();
	            System.out.println("DataStore::dataOccurred: got "+fLastData.size()+" paramters");
	            
	        }
	    }
	    //________________________________________________________________________________

}
