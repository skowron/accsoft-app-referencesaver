package cern.accsoft.op.referencesaver.co;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cern.accsoft.op.referencesaver.Config;
import cern.japc.core.Parameter;
import cern.japc.core.ParameterException;
import cern.japc.core.Selector;
import cern.japc.core.factory.ParameterFactory;
import cern.japc.core.factory.SelectorFactory;
import cern.japc.core.factory.SimpleParameterValueFactory;
import cern.japc.value.ParameterValue;

public class JapcUtils {

	protected static final Logger LOGGER = LoggerFactory.getLogger(JapcUtils.class);
	
	public static void setNewReference(String refName, String tgmCycle) 
	{
		Parameter p;
		try {
			p = ParameterFactory.newInstance().newParameter(Config.getReferenceNameParam());
		} catch (ParameterException e) {
			LOGGER.error("Cannot find parameter named {}",Config.getReferenceNameParam());
			return;
		}
		 
		 Selector s = SelectorFactory.newSelector(tgmCycle);
		 //produces a simple value with an int inside
		 ParameterValue pv = SimpleParameterValueFactory.newSimpleParameterValue(refName);
		 try {
			p.setValue(s, pv);
		} catch (ParameterException e) {
			LOGGER.error("Error while setting {}",Config.getReferenceNameParam());
	       }
	}
}
