/**
 * Copyright (c) 2024 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.accsoft.op.referencesaver.nxcals;

import org.apache.spark.sql.Row;
import org.apache.spark.sql.types.StructField;

import cern.japc.core.factory.MapParameterValueFactory;
import cern.japc.json.value.map.MapParameterValueDto;
import cern.japc.value.MapParameterValue;


public class ParameterBuilder {

    public MapParameterValue get(Row row, StructField[] fields)
    {
        MapParameterValue mpv = MapParameterValueFactory.newMapParameterValue();
        MapParameterValueDto dto = null ;
        
        
        return mpv;
    }
}
