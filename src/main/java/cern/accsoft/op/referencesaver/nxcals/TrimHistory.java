package cern.accsoft.op.referencesaver.nxcals;

import java.time.Instant;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cern.accsoft.ccs.ccda.client.model.accelerator.Accelerator;
import cern.cmw.datax.converters.DataxToJapcConverter;
import cern.japc.value.MapParameterValue;
import cern.nxcals.api.converters.SparkRowToImmutableDataConverter;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.extraction.metadata.EntityService;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.SystemSpecService;
import cern.nxcals.api.metadata.queries.ConditionWithOptions;
import cern.nxcals.api.metadata.queries.Entities;
import cern.nxcals.api.metadata.queries.EntityQueryWithOptions;

import cern.nxcals.api.extraction.data.builders.DataQuery;

public class TrimHistory {

	static Logger LOGGER = LoggerFactory.getLogger(Tagging.class);
	
    SystemSpecService fSystemService;
    EntityService fEntityService;
    Accelerator fAccelerator;
    
    private SparkSession fSparkSession = null;

    public TrimHistory( SparkSession sesssion)
    {
        fSparkSession = sesssion;
        
        fSystemService = ServiceClientFactory.createSystemSpecService();
        fEntityService = ServiceClientFactory.createEntityService();

    }
    
    public TreeMap<Date, MapParameterValue> getTrims(String parName, String lsaCycleName)
    {
    	
    	TimeWindow twin = TimeWindow.between(Instant.MIN, Instant.now() );
    	TreeMap<Date, MapParameterValue> retval = new TreeMap<Date, MapParameterValue>();
    	
    	Collection<Entity> list = findParemeterEntityByNameAndContext(parName, lsaCycleName);
    	
    	if ( list.isEmpty() )
    	{
    		LOGGER.info("Did not find any entities for {} {}", parName, lsaCycleName);
    		
    		return retval;
    	}
    		
    	Entity ent0 = list.iterator().next();
    	Dataset<Row> dataset = DataQuery.getFor(fSparkSession, twin, ent0);
    	
    	//System.out.println( "getTrims: Entity key values " + ent0.getEntityKeyValues() );
    	
    	List<Row> data = dataset.collectAsList();
        int i=0;
        for (Row row: data)
        {
            //LOGGER.info("getDataFromTag:      row  {} ", i);
            MapParameterValue mpv = DataxToJapcConverter.toMapParameterValue(SparkRowToImmutableDataConverter.convert(row));
            
            long ts = mpv.getLong("__record_timestamp__");
            Date trimtime = Date.from(Instant.ofEpochSecond(0, ts));
            
            if (mpv.get("value") == null)
            {
            	//LOGGER.info("getDataFromTag:  trim on {} has no value", trimtime);
            	mpv.setString("value", "");
            }
            
            System.out.println("mpv: "+ trimtime +" \n"+ mpv);
            retval.put(trimtime, mpv);
            i++;
        }
    	 
        return retval;
        
//    	trims = DataQuery.builder(spark).byEntities() \
//                .system('SETTINGS') \
//                .startTime(t0) \
//                .endTime(t1) \
//                .entity().keyValues({'context':'PHYSICS-6.5TeV-30cm-120s-2018_V1@120_[END]', 'parameter':'LHCBEAM1/IP1_SEPSCAN_X_MM'}) \
//                .buildDataset()
    	
    	
    	
    }
    
    
    private Collection<Entity> findParemeterEntityByNameAndContext(String namelike, String lsaCycleName ) {
        
    	System.out.println("findParemeterEntityByNameAndContext "+ namelike + " "+ lsaCycleName);
    	
        SystemSpec system = fSystemService.findByName("SETTINGS").orElseThrow(() -> new RuntimeException("Can't find SETTINGS system"));
        
        Collection<Entity> result = null;
        
        if (true)
        {
        EntityQueryWithOptions query2 = cern.nxcals.api.metadata.queries.Entities.suchThat().systemId().eq(system.getId()).and()
                .keyValues()
                .like(system, Map.of("parameter", namelike, "context", lsaCycleName)).withOptions().noHistory();
        result = fEntityService.findAll(query2);
        }
        else
        {
         ConditionWithOptions<Entities, EntityQueryWithOptions> query1 = cern.nxcals.api.metadata.queries.Entities.suchThat().systemId().eq(system.getId()).and()
                .keyValues()
                .eq(system, Map.of("parameter", namelike, "context", lsaCycleName));
        
         result = fEntityService.findAll(query1);
        }

        System.out.println("Found {} devices matching {} "+ result.size() + " "+ namelike);
        
        LOGGER.info("Found {} devices matching {} ",result.size(), namelike);
        
        return result; 
        
    }

}
