/**
 * Copyright (c) 2024 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.accsoft.op.referencesaver.nxcals;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.SparkSession.Builder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import cern.accsoft.op.referencesaver.ReferenceSaverGuiApp;

public class Nxcals {

    static Logger log = LoggerFactory.getLogger(Nxcals.class);
    
    private static Map<String,Object> getConfig()
    {
       ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
       mapper.findAndRegisterModules();
       
       URL fileurl = ReferenceSaverGuiApp.class.getResource("/application.yml");
       Map<String,Object> conf = null;
       
       
       try {
           conf = mapper.readValue(fileurl, Map.class);
        } catch (IOException e) {
        
          e.printStackTrace();
          return conf;
        }
       
       HashMap<String,Object> flatMap = new HashMap<>(100);
       
       return conf;
       
    }
    
    public static SparkSession getSparkSession() {

     Map<String,Object> conf = getConfig();
        
     
     Builder sparkBuilder = SparkSession.builder().
                appName("Reference Saver");
     
     

    for ( Map.Entry<String,Object> e: conf.entrySet())
     {
         System.out.println(e.getKey() +" "+e.getValue());
     }

     SparkSession sparkSession =   sparkBuilder.getOrCreate();

        log.info("Got spark session {}", sparkSession);
        return sparkSession;
    }
    
    
}
