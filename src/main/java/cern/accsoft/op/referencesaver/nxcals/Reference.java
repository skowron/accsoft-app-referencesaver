package cern.accsoft.op.referencesaver.nxcals;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cern.accsoft.gui.frame.MessageManager;
import cern.accsoft.op.referencesaver.ReferenceSaverGuiApp;
import cern.japc.core.spi.ParameterUrlImpl;
import cern.japc.value.MapParameterValue;
import cern.japc.value.SimpleParameterValue;
import cern.nxcals.api.custom.domain.Tag;
import cern.nxcals.api.domain.Entity;
import lombok.NonNull;

public class Reference {
	/**
	 * Object representing reference. 
	 * For NXCALS reference is a @Tag. 
	 * This class ads functionalities to retrieve data from NXCALS in an easy way
	 * It is used mainly by ReferenceViewPanel that visualizes data provided by this clss
	 */
	protected static final Logger LOGGER = LoggerFactory.getLogger(Reference.class);
	
	String fName;

	Tag   fTag;
	Set<Entity> fEntities = null;
	Map<Entity,List<MapParameterValue>> fValues = new HashMap<>();
	

    public  List<MapParameterValue> getValues(Entity ent)
	{
    	if (ent == null)
    	{
    		return null;
    	}
    	
		if (fValues.get(ent) != null)
		{
			return fValues.get(ent);
		}
		
		Tagging tagsrv = ReferenceSaverGuiApp.getInstance().getTaggingService();
		List<MapParameterValue> vals = null;
		synchronized (this)
		{
	   	   vals = tagsrv.getDataFromTagForEntity(fTag, ent);
		}
		fValues.put(ent,vals);
		
		return vals;
	}
	//____________________________________________________________________
    public MapParameterValue getValue(String deviceName, String propertyName)
    {
    	/**
    	 * returns fist value only. Reference sometime have more than one, but this is strange parameters that trigger more than ones per cycle
    	 */
    	Entity ent = getEntityByDevPropNames(deviceName, propertyName);
    	if (ent == null)
    	{
    	  LOGGER.warn("getValue(String,String): Entity is null for {} {}",deviceName, propertyName );
    	  return null;
    	}
    	
    	List<MapParameterValue> lv = getValues(ent);
    	
    	if (lv == null || lv.isEmpty())
    	{
    		LOGGER.warn("getValue(String,String): No values for {} {}",deviceName, propertyName );
    		return null;
    	}
    	
    	return lv.get(0);
    }
    // 
    public Tag getTag()
    {
    	return fTag;
    }
	//____________________________________________________________________
    public SimpleParameterValue getValue(String deviceName, String propertyName, String fieldName)
    {
    	/**
    	 * returns fist value only. Reference sometime have more than one, but this is strange parameters that trigger more than ones per cycle
    	 */
    	System.out.println("Reference::getValue: looking for ref value "+deviceName+" / "+propertyName);
    	MapParameterValue mpv = getValue(deviceName, propertyName);
    	if (mpv == null)
    	{
    		System.out.println("Reference::getValue: looking for ref value "+deviceName+" / "+propertyName+" DID NOT FIND!");
    		LOGGER.warn("getValue: could not find reference value for {} / {} # {}",deviceName,propertyName, fieldName);
    		return null;
    	}
    	
    	System.out.println("Reference::getValue: got ref value "+deviceName+" / "+propertyName+", looking for field "+ fieldName);
    	SimpleParameterValue spv = mpv.get(fieldName);
    	if (spv == null)
    	{
    		LOGGER.warn("Reference::getValue: {} / {} does not have field {} ", deviceName,propertyName, fieldName);
    	    System.err.println("Reference::getValue: ref value "+deviceName+" / "+propertyName+" does not have field "+ fieldName);
    	}
    	
    	return spv;
    }
	//____________________________________________________________________
    public SimpleParameterValue getValue(String paramterName)
    {
    	ParameterUrlImpl parurl = new ParameterUrlImpl(paramterName);
    	
        return getValue(parurl.getDeviceName(), parurl.getPropertyName(), parurl.getFieldName());
    }
    
	//____________________________________________________________________
    
    public Entity getEntityByDevPropNames(String deviceName, String propertyName)
    {
    	for (Entity ent: fEntities)
    	{
    		@NonNull
			Map<String, Object> kvals = ent.getEntityKeyValues();
    		if ( !kvals.get("device").equals(deviceName) )
    		{
    			continue;
    		}
    		if ( !kvals.get("property").equals(propertyName) )
    		{
    			continue;
    		}
    		return ent;
    	}
    	return null;
    }
   //____________________________________________________________________
	public Reference(String name)
	{
		fName = name;
	}
	//____________________________________________________________________	
	public Set<Entity> getEntities() {
		return fEntities;
	}
	public void setEntities(Set<Entity> fEntities) {
		this.fEntities = fEntities;
	}
	
	//_______________________________________________________________________
	
	public int retrieve()
	{
		System.out.println("ReferenceViewPanel::retrieve "+fName); 
		Tagging tagsrv = ReferenceSaverGuiApp.getInstance().getTaggingService();
		
		
		try
		{
		  fTag = tagsrv.getTag(fName);
		}
		catch(Exception ex)
		{
			System.err.println("ReferenceViewPanel::retrieve: Exception while getting tag "+fName+" "+ex.getMessage());
			ex.printStackTrace();
			return 0;
		}
		
		fEntities = tagsrv.getEntitiesFromTag(fTag);
		if (fEntities == null)
		{
			System.err.println("ReferenceViewPanel::retrieve: No entities for tag "+fName);
			return 0;
			
		}
		System.out.println("ReferenceViewPanel::retrieve "+fName+" has "+fEntities.size()+" entities");
		
		return fEntities.size();
	}
	
	public double[] getArray(String parameter)
	{
    	double[] retval = new double[] {1,2,3,4,5,6,7,8,9,10};  
    	return retval;
  	
	}
	
	public String getName() {
		return fName;
	}

	public void setName(String fName) {
		this.fName = fName;
	}

}
