/**
 * Copyright (c) 2024 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.accsoft.op.referencesaver.nxcals;

import static cern.nxcals.api.custom.extraction.metadata.TagService.DEFAULT_RELATION;
import static cern.nxcals.api.extraction.data.builders.DataQuery.builder;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.StructField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.rutledgepaulv.qbuilders.conditions.Condition;
import com.google.common.collect.ImmutableMap;

import cern.accsoft.ccs.ccda.client.core.search.SearchBuilder;
import cern.accsoft.ccs.ccda.client.core.search.SearchOperator;
import cern.accsoft.ccs.ccda.client.model.accelerator.Accelerator;
import cern.accsoft.ccs.ccda.client.model.device.DeviceNameAndAlias;
import cern.accsoft.ccs.ccda.client.model.device.query.DeviceQueryField;
import cern.accsoft.commons.ccs.CcdaUtils;
import cern.cmw.datax.converters.DataxToJapcConverter;
import cern.japc.value.MapParameterValue;
import cern.nxcals.api.converters.SparkRowToImmutableDataConverter;
import cern.nxcals.api.custom.domain.Tag;
import cern.nxcals.api.custom.extraction.metadata.TagService;
import cern.nxcals.api.custom.extraction.metadata.TagServiceFactory;
import cern.nxcals.api.custom.extraction.metadata.queries.Tags;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.Visibility;
import cern.nxcals.api.extraction.data.builders.DataQuery;
import cern.nxcals.api.extraction.metadata.EntityService;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.SystemSpecService;
import cern.nxcals.api.extraction.metadata.queries.Entities;
import cern.nxcals.api.metadata.queries.EntityQueryWithOptions;
import cern.nxcals.api.utils.TimeUtils;
import lombok.SneakyThrows;

public class Tagging {
    
    static Logger log = LoggerFactory.getLogger(Tagging.class);
    
    private TagService fTagService = null;
    
    SystemSpecService fSystemService;
    EntityService fEntityService;

    
    private SparkSession fSparkSession = null;
    
    private List<DeviceNameAndAlias> fDevices = null;
    Accelerator  fAccelerator = null;

    
    
    public Tagging( SparkSession sesssion, Accelerator accel)
    {
        fSparkSession = sesssion;
        
        fTagService = TagServiceFactory.createTagService();
        fSystemService = ServiceClientFactory.createSystemSpecService();
        fEntityService = ServiceClientFactory.createEntityService();

        fAccelerator = accel;

    }
    //________________________________________________________
    //
    
    @SneakyThrows
    public Tag createTag(String theTag, long timesamp) {

        if (fDevices == null)
         {
           String query = SearchBuilder.search().predicate(DeviceQueryField.accelerator, SearchOperator.EQUAL, fAccelerator.getName()).build();
           fDevices = CcdaUtils.getDeviceService().searchDeviceNames(query);
         }
        
        System.out.println("Tagging::createTag: found "+fDevices.size() + " devices");

        
        
        //select some timestamp for your TAG
        long tagTimestamp = findSomeTimestamp();

        
        List<Entity> entities = getAllEntities();
        
        System.out.println("Tagging::createTag: Found device/props: " + entities.size());
        //for (Entity entity: entities)
        //  {
        //    System.out.println("entit "+entity);
        //  }
        
        if (entities.isEmpty())
        {
        	System.out.println("Tagging::createTag: No entities found to tag matching the criteria");
            return null;
        }

        //create a TAG
        Tag tag = fTagService.create(theTag,
                "description", Visibility.PUBLIC,
                "skowron", // user name 
                "CMW",   // system
                Instant.ofEpochSecond(0, tagTimestamp),
                entities, 
                Collections.emptyList()); // variables

        
        log.info("############# TAG CREATED {} ", tag.getName());
        System.out.println("Tagging::createTag: ############# TAG CREATED {} " +  tag.getName());
        return tag;
    }
    
    //________________________________________________________

    //Finding some random timestamp from a Fundamental.
    public long findSomeTimestamp() {
        Dataset<Row> fundamentals = builder(fSparkSession).variables().system("CMW").nameEq("PSB:NXCALS_FUNDAMENTAL")
                .timeWindow("2024-04-24 14:00:00", "2024-04-24 14:01:00").build();

        return fundamentals.select("nxcals_timestamp").first().getLong(0);
    }
    //________________________________________________________
    //Finding some random device/properties from CMW system.
    private  Set<Entity> findEntitiesByName2(String namelike) {
        
        log.info("::findEntitiesByName: looking for {}", namelike);
        return fEntityService.findAll(
                Entities.suchThat().keyValues().caseInsensitive().like(namelike).and().systemName().eq("CMW"));
    }
    //________________________________________________________
    
    private List<Entity> findEntitiesByName(String namelike) {
        
        SystemSpec system = fSystemService.findByName("CMW").orElseThrow(() -> new RuntimeException("Can't find CMW system"));
        boolean newWay = true;
        List<Entity> entities = null;
        if (newWay)
        {
	         long histStart = TimeUtils.getNanosFromString("2015-10-10 14:15:00.000000000");
	         long histEnd = TimeUtils.getNanosFromInstant(Instant.now());
	          
//	         EntityQueryWithOptions query1 = cern.nxcals.api.metadata.queries.Entities.suchThat()
//	        		                        .systemId().eq(2L)
//	        		                        .and().keyValues().like(system, Map.of("device", namelike, "property", "%"))
//	        		                        .withOptions()
//	        		                        .withHistory(histStart, histEnd);
//	         entities = fEntityService.findAll(query1);

              System.out.println("Looking for namelike "+namelike);
	          Condition<Entities> query2 = cern.nxcals.api.extraction.metadata.queries.Entities.suchThat()
                     .systemId().eq(2L)
                     .and()
                     .keyValues().like(system, Map.of("device", namelike, "property", "%"));

              entities = new ArrayList<Entity>();

              Set<Entity> set =  fEntityService.findAllWithHistory(query2,histStart,histEnd);
              entities.addAll(set);

	          //Optional<Entity> res = fEntityService.findOneWithHistory(query2,histStart,histEnd);
	          //if (res.isPresent())	entities.add(res.get());
	        	  
        }
        else
        {
	        EntityQueryWithOptions query2 = cern.nxcals.api.metadata.queries.Entities.suchThat()
                    .systemId().eq(2L).and()
                    .keyValues().like(system, Map.of("device", namelike, "property", "%"))
                    .withOptions().noHistory();

            entities = fEntityService.findAll(query2);
        }
        log.info("Found {} devices matching {} ",entities.size(), namelike);
        //System.out.println((end.toEpochMilli() - start.toEpochMilli()));
        
        return entities; 
        
    }
    //________________________________________________________
    // gets data for devicese following standard patterns L4L.%, L4X.%
    // and removes them from the list
    private List<Entity> getEntitiesForSections(List<String> groups, List<DeviceNameAndAlias> devs)
    {
        List<Entity> ret = new ArrayList<>();
        for (String group: groups)
        {
        	List<Entity> found = findEntitiesByName("%"+group+"%");
        	System.out.println("getEntitiesForSections: found for "+group+" "+found.size()+ " entities");
        	found.removeIf(s -> !((String)s.getEntityKeyValues().get("device")).startsWith(group));
        	
            ret.addAll( found );
            devs.removeIf(s -> s.getName().startsWith(group));
            
        }
        return ret;
    }
    //________________________________________________________

    private List<Entity> getAllEntities() {
      
      ArrayList<DeviceNameAndAlias> devs = new ArrayList<>(fDevices);
      
      List<String> groups = Arrays.asList("L4.","LN4.",
                                          "L4X.",
                                          "L4L.","L4C.","L4D.","L4P.","L4T.","LT.","LTB.","LBE.",
                                          "CIBM.400.LN4.","CIBX.400.LN4");
      List<Entity> ret = getEntitiesForSections(groups, devs);
      devs.removeIf(s -> s.getName().endsWith("-OLD"));
      devs.removeIf(s -> s.getName().startsWith("L4LT"));
      devs.removeIf(s -> s.getName().startsWith("L4TX"));
      devs.removeIf(s -> s.getName().startsWith("GD"));
      

      for (DeviceNameAndAlias device: devs)
        {

           //log.info("::getAllEntities: looking for entities for device "+ device.getName()+ " ...");
           //Entities.suchThat().keyValues().caseInsensitive().equals(device.getName());
           
           List<Entity> l = findEntitiesByName("%"+device.getName()+"%");
           //log.info("::getAllEntities: looking for entities for device "+ device.getName()+ " ... Found "+l.size());
           System.out.println("::getAllEntities: looking for entities for device "+ device.getName()+ " ... Found "+l.size());
           if(ret == null)
           {
               ret = l;
           }
           else
           {
               ret.addAll(l);
           }
           
        }
       return ret; 
    }

    //________________________________________________________
    public boolean isTagExisting(String tagName)
    {
    	Optional<Tag> retval = fTagService.findOne(Tags.suchThat().name().eq(tagName));
    	
        return retval.isPresent();
    }
    
    //________________________________________________________
    public Tag getTag(String tagName)
    {
        return fTagService.findOne(Tags.suchThat().name().eq(tagName))
        .orElseThrow(() -> new RuntimeException("No such tag: " + tagName));
    }
    //________________________________________________________

    @SneakyThrows
    public  void findAndUpdateTag( String tagName, String newDescription) {
        //Find a TAG
        Tag tag = getTag(tagName);

        //Update TAG information
        Tag newTag = tag.toBuilder().description(newDescription).properties(ImmutableMap.of("new Key", "value")).build();


        newTag = fTagService.update(newTag);



        tag = fTagService.findOne(Tags.suchThat().name().eq(tagName))
                .orElseThrow(() -> new RuntimeException("No such tag: " + tagName));

        log.info("############# TAG FOUND AND UPDATED {} ", tag);

    }
    //________________________________________________________
    public Set<Entity> getEntitiesFromTag(Tag tag)
    {
    	System.out.println();
    	return fTagService.getEntities(tag.getId()).get(DEFAULT_RELATION);
    }

    public  List<MapParameterValue> getDataFromTagForEntity(Tag tag, Entity ent) 
    {
    	LinkedList<MapParameterValue> retval = new LinkedList<MapParameterValue>();
    	
        Dataset<Row> dataset = DataQuery.builder(fSparkSession)
                						.entities()
						                .idEq(ent.getId())
						                .timeWindow(tag.getTimestamp(), tag.getTimestamp().plusNanos(1199999999L))
						                .build();
        log.info("::getDataFromTag: Datat by time range");

        
        if(dataset.isEmpty()) 
        {
           return retval;
        }
        log.info("getDataFromTag:      DataSet for {}", ent.getEntityKeyValues());
        dataset.show();
        //Dataset<Row> data = dataset.select("device", "property", "cyclestamp", "selector");


        StructField[] fields = dataset.schema().fields();

        List<Row> data = dataset.collectAsList();
        int i=0;
        for (Row row: data)
        {
        	log.info("getDataFromTag:      row  {} ", i);
        	MapParameterValue mpv = DataxToJapcConverter.toMapParameterValue(SparkRowToImmutableDataConverter.convert(row));
        	retval.add(mpv);
        	System.out.println("mpv: "+ mpv);
        	i++;
        }


        return retval;   
        
    }
    
    public  void getDataFromTag(  Tag tag) {
        //Getting Entities from a TAG

        log.info("");
        System.out.println("     EEEEEEEEEEEEEEEEEEE");
        System.out.println("     E X T R A C T I O N ");
        System.out.println("     XXXXXXXXXXXXXXXXXXX");
        log.info("");
        Set<Entity> entities = getEntitiesFromTag(tag);
        
        log.info("getDataFromTag: entities  is null ?: {}", entities == null);
        if (entities == null)
        {
            log.error("getDataFromTag: Did not find any entities for tag {} ", tag);
            return;
        }
        
        log.info("getDataFromTag: entities  : {}", entities.size());
        
        //Querying for DATA of those Entities
        for(Entity ent : entities) {
            System.out.println("");
            log.info("");
            log.info("::getDataFromTag: Entity data " + ent.getEntityKeyValues() + " at time: " + tag.getTimestamp());

            Dataset<Row> dataset = DataQuery.builder(fSparkSession)
                    .entities()
                    .idEq(ent.getId())
                    .atTime(tag.getTimestamp()).build();
            log.info("::getDataFromTag: Datat by time exact");
            dataset.show();
            
            //if(dataset.isEmpty())
            {
                dataset = DataQuery.builder(fSparkSession)
                        .entities()
                        .idEq(ent.getId())
                        .timeWindow(tag.getTimestamp(), tag.getTimestamp().plusNanos(1199999999L))
                        .build();
                log.info("::getDataFromTag: Datat by time range");
                dataset.show();
            }
            
            if(!dataset.isEmpty()) {
                log.info("getDataFromTag:      DataSet for {}", ent.getEntityKeyValues());
                dataset.show();
                //Dataset<Row> data = dataset.select("device", "property", "cyclestamp", "selector");
                
                
                StructField[] fields = dataset.schema().fields();
                
                List<Row> data = dataset.collectAsList();
                int i=0;
                for (Row row: data)
                {
                    log.info("getDataFromTag:      row  {} ", i);
                    MapParameterValue mpv = DataxToJapcConverter.toMapParameterValue(SparkRowToImmutableDataConverter.convert(row));
                    System.out.println("mpv: "+ mpv);
                    i++;
                }
                
                
                
            } else {
                log.info("???????????? NO Data for {}", ent.getEntityKeyValues());
            }
        }

    }    
 
    public void deleteTag(Tag tag)
    {
        fTagService.delete(tag.getId());
    }
}
