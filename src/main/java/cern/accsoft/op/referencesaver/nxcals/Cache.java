package cern.accsoft.op.referencesaver.nxcals;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import cern.accsoft.op.referencesaver.Config;

public class Cache {
	
	protected final Logger LOGGER = LoggerFactory.getLogger(getClass());
	
	
	private static Cache fgkInstance = null;
	
	public static Cache Instance()
	{
      if (fgkInstance == null) 
		{
			fgkInstance = new Cache();
		}
	  
      return fgkInstance;
	
	}
	
	private File getDevPropFieldFile() throws IOException
	{
		String home = System.getenv("HOME");
		Path path = Paths.get(home,".ReferenceSaver", Config.getAcceleratorName());
		Files.createDirectories(path);
		path = path.resolve("DevPropFields.json");
		
		return  path.toFile(); 
	}
	
	public void saveDevPropFields(Map<String, List<String>> eps)
	{
		ObjectMapper objectMapper = new ObjectMapper();
		//DefaultPrettyPrinter pprinter = new DefaultPrettyPrinter();
		//ObjectWriter writer = objectMapper.writer(pprinter);
	    //String jacksonData;
//		try {
//			jacksonData = objectMapper.writeValueAsString(eps);
//		} catch (JsonProcessingException e) {
//			LOGGER.error("Error converting the entity-params to JSON", e);
//			e.printStackTrace();
//			return;
//		}
	    
	    
	    try {
			objectMapper.writerWithDefaultPrettyPrinter().writeValue(getDevPropFieldFile(),eps);
			//writer.writeValue(getDevPropFieldFile(), jacksonData);
		} catch (Exception e) {
			LOGGER.error("Error writing the entity-params to JSON", e);
			
			e.printStackTrace();
		} 
	
	}
	
	public Map<String, List<String>> readDevPropFields()
	{
		Map<String, List<String>>  retval = null;
		
		File f = null;
		try {
			f = getDevPropFieldFile();
			if(!f.exists() || f.isDirectory()) { 
			    return retval; 
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			retval = objectMapper.readValue(f,  new TypeReference<Map<String,List<String>>>(){} );
		} 
		catch (Exception e) 
		{
			LOGGER.error("Error reading the entity-params from JSON", e);
			e.printStackTrace();
		} 
		return retval;
	}
}
