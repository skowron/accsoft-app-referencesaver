package cern.accsoft.op.referencesaver.nxcals;

import static org.apache.spark.sql.functions.broadcast;

import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeMap;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;

import cern.accsoft.op.referencesaver.Config;
import cern.accsoft.op.referencesaver.gui.treeobjects.EntityParam;
import cern.accsoft.op.referencesaver.gui.treeobjects.EntityParam.Type;
import cern.cmw.datax.converters.DataxToJapcConverter;
import cern.japc.value.MapParameterValue;
import cern.nxcals.api.converters.SparkRowToImmutableDataConverter;
import cern.nxcals.api.domain.Entity;
import cern.nxcals.api.domain.EntityHistory;
import cern.nxcals.api.domain.EntitySchema;
import cern.nxcals.api.domain.SystemSpec;
import cern.nxcals.api.domain.TimeWindow;
import cern.nxcals.api.extraction.data.builders.DataQuery;
import cern.nxcals.api.extraction.metadata.EntityService;
import cern.nxcals.api.extraction.metadata.ServiceClientFactory;
import cern.nxcals.api.extraction.metadata.SystemSpecService;
import cern.nxcals.api.extraction.metadata.queries.Entities;
import cern.nxcals.api.metadata.queries.EntityQueryWithOptions;
import cern.nxcals.api.utils.TimeUtils;
import lombok.NonNull;

public class Extractor {

    private SparkSession fSparkSession = null;

    SystemSpecService fSystemService;
    EntityService fEntityService;
    SystemSpec fSystemSpecCMW;
    
    private EntityParam.Type fLastValuesType = EntityParam.Type.UNDEFINED;
    
    
    //____________________________________________________________________________
    public Extractor( SparkSession sesssion)
    {
        fSparkSession = sesssion;
        
        fSystemService = ServiceClientFactory.createSystemSpecService();
        fEntityService = ServiceClientFactory.createEntityService();

    	fSystemSpecCMW = fSystemService.findByName("CMW").orElseThrow(() -> new RuntimeException("Can't find CMW system"));

    }
    //____________________________________________________________________________
    
    public static TreeMap<Date, MapParameterValue> sparkDatasetToMapParameterValues(Dataset<Row> dataset)
    {
    	TreeMap<Date, MapParameterValue> retval = new TreeMap<Date, MapParameterValue>();
    	List<Row> data = dataset.collectAsList();
    	System.out.println("sparkDatasetToMapParameterValues: got no of entries: "+data.size());
    	
        for (Row row: data)
        {
            //LOGGER.info("getDataFromTag:      row  {} ", i);
            MapParameterValue mpv = DataxToJapcConverter.toMapParameterValue(SparkRowToImmutableDataConverter.convert(row));
            
            long ts = mpv.getLong("__record_timestamp__");
            Date trimtime = Date.from(Instant.ofEpochSecond(0, ts));
            
            if (mpv.get("value") == null)
            {
            	//LOGGER.info("getDataFromTag:  trim on {} has no value", trimtime);
            	mpv.setString("value", "");
            }
            
            //System.out.println("mpv: "+ trimtime +" \n"+ mpv);
            retval.put(trimtime, mpv);
        }
        
        return retval;
    }
    //____________________________________________________________________________
    
    public TreeMap<Date, MapParameterValue> getValues(String deviceName, String propertyName, String lsaCycleName, TimeWindow twin)
    {
    	
    	System.out.println("getValues ");
    	
    	fLastValuesType = Type.UNDEFINED;
    	
    	Collection<Entity> list = findEntitiesByDeviceAndProperty(deviceName, propertyName);
    	
    	if ( list.isEmpty() )
    	{
    		System.err.println("Did not find any entities for "+ deviceName+"/"+propertyName);
    		
    		return  new TreeMap<Date, MapParameterValue>();

    	}
    	
    	if (list.size() > 1)
    	{
    		System.err.println("Warning, got more than one entity ("+list.size()+") for "+ deviceName+"/"+propertyName);
    		System.err.println("Did not expect it, using only 1st one"); 
    	}
    	
    	Entity ent0 = list.iterator().next();
    	
    	Dataset<Row> dataset = DataQuery.getFor(fSparkSession, twin, ent0);
    	Dataset<Row> fund = findLsaCycleFundamentals(twin, lsaCycleName);
    	Dataset<Row> data_filtered = dataset.join(broadcast(fund), "__record_timestamp__");
    	
    	TreeMap<Date, MapParameterValue> retval = sparkDatasetToMapParameterValues(data_filtered);
    	
    	if (!retval.isEmpty())
    	{
    		fLastValuesType = Type.ACQUISITION_CYCLEBOUND;
    		return retval;
    	}
    	System.out.println("getValues: looks like data stored with ACQUISITION timestamp ");

    	//fund.printSchema();
    	//dataset.printSchema();

    	TreeMap<Date, MapParameterValue> fund_mpvs = sparkDatasetToMapParameterValues(fund);
    	TreeMap<Date, MapParameterValue> data_mpvs = sparkDatasetToMapParameterValues(dataset);

    	retval = joinByAqnStamp(fund_mpvs, data_mpvs);


    	System.out.println("getValues: by ACQUISITION found records "+ retval.size());
    	if (retval.size() > 0  )
    	{
    		double fracOfPulses = Math.abs(fund_mpvs.size() - retval.size())/fund_mpvs.size();
    		if ( fracOfPulses > 0.9 )
    		{
    			fLastValuesType  = Type.ACQUISITION_NON_CYCLEBOUND;
    		}
    		else
    		{
    			fLastValuesType = Type.SETTING_NON_LSA;
    		}
    		
    	}
    	
    	return retval;
    	
    }
    //____________________________________________________________________________
    
    TreeMap<Date, MapParameterValue> joinByAqnStamp(TreeMap<Date, MapParameterValue> fund, TreeMap<Date, MapParameterValue> data)
    {
       /**
        * We take data that have Acquisition Time samp between fundamental's cyclestamp and cyclestamp+cycleduration  
    	 * Left is fundamentals
    	 * Right is data
    	 * Loop over fun
        */
       TreeMap<Date, MapParameterValue> retval = new TreeMap<Date, MapParameterValue>();
       
       NavigableSet<Date> fund_ks = fund.navigableKeySet();
       NavigableSet<Date> data_ks = data.navigableKeySet();
       Iterator<Date> data_it = data_ks.iterator();
       
       Date data_nxts = data_it.next();
	   MapParameterValue data_mpv = data.get(data_nxts);
	   long data_cts = data_mpv.getLong("__record_timestamp__");// should be the same as acquisition stamp
       
       for (Date fund_nxts: fund_ks)
       {
    	   MapParameterValue fund_mpv = fund.get(fund_nxts);
    	   long fund_cts = fund_mpv.getLong("__record_timestamp__");// should be the same as cyclestamp
    	   long fund_cendts = fund_cts + fund_mpv.getLong("CYCLE_DURATION_MS")*1000000;
    	   
    	   do 
    	   {
    	     // 1. rewind until data get newer then fundamental  
    		   
    		   if (data_cts < fund_cts)
    		   {
    			   if (!data_it.hasNext())
    			   {
    				   return retval;// end of data, we are done
    			   }
    			   data_nxts = data_it.next();
    			   
        		   data_mpv = data.get(data_nxts);
            	   data_cts = data_mpv.getLong("__record_timestamp__");// should be the same as acquisition stamp

    			   continue;
    		   }
    		   
    		   // here we have data newer than fundamental 
    		   if (data_cts < fund_cendts)
    		   {
    			   retval.put(fund_nxts, data_mpv);
    			   if (!data_it.hasNext())
    			   {
    				   return retval;// end of data, we are done
    			   }
    			   data_nxts = data_it.next();
    		   }
    		   // this data is newer than the current fundamental
    		   // we take next fundamental and continue
    		   break;
    		   
    	   }while (data_it.hasNext() );
    	  // here we can take next fundamental
       }
       
       return retval;
    }
    
    private List<Entity> findEntitiesByDeviceAndProperty(String deviceName, String propertyName) {
        
    	System.out.println("findEntitiesByDeviceAndProperty: ");
        
        
        
        
        EntityQueryWithOptions query2 = cern.nxcals.api.metadata.queries.Entities.suchThat().systemId().eq(2L).and()
                .keyValues()
                .like(fSystemSpecCMW, Map.of("device", deviceName, "property", propertyName)).withOptions().noHistory();

        
        
        //Instant start = Instant.now();
        List<Entity> entities2 = fEntityService.findAll(query2);
        //Instant end = Instant.now();
        
        System.out.println("findEntitiesByDeviceAndProperty: Found no "+entities2.size());
        
        return entities2; 
        
    }
    //____________________________________________________________________________

    public Dataset<Row> findFundamentals(TimeWindow timewin)
    {

    	Map<String, Set<String>> fieldAliases = new HashMap<>();
    	fieldAliases.put("LSA_CYCLE", Set.of("__LSA_CYCLE__","lsaCycleName"));

    	Dataset<Row> funds = DataQuery.builder(fSparkSession).variables()
    			   .system("CMW")
                   .nameEq(Config.getFundamentalVariable())
                   .timeWindow(timewin)
                   .fieldAliases(fieldAliases)
                   .build();

    	return funds;

    }
    //____________________________________________________________________________
    
    public Dataset<Row> findLsaCycleFundamentals(TimeWindow timewin, String lsaCycleName)
    {
    	Dataset<Row> funds = findFundamentals(timewin);
    	Dataset<Row> funds_filtered = funds.select("nxcals_timestamp","CYCLE_DURATION_MS").where("LSA_CYCLE = '"+lsaCycleName+"'");
    	//Dataset<Row> funds_filtered = funds.where("LSA_CYCLE = '"+lsaCycleName+"'");
    	
    	return funds_filtered.withColumnRenamed( "nxcals_timestamp", "__record_timestamp__");
      
    }
    
    public Map<String, String> getFields(String device, String property, Instant ts)
    {
    	
    	Map<String, Object> keyValues = ImmutableMap.of("device", device, "property", property);

        
        Entity entities = fEntityService.findOneWithHistory(
                Entities.suchThat().systemName().eq(fSystemSpecCMW.getName()).and().keyValues().eq(fSystemSpecCMW, keyValues),
                TimeUtils.getNanosFromString("2015-10-10 14:15:00.000000000"),
                TimeUtils.getNanosFromInstant(Instant.now())
        ).orElseThrow(() -> new IllegalArgumentException("Entity not found"));


        Map<String,String> retval = new TreeMap<String, String>();
        
        entities.getEntityHistory().forEach(entityHistory -> {
            if (entityHistory.getValidity().contains(ts))
            {
			   Map<String, String> l = getFieldsFromSchema(entityHistory.getEntitySchema());
               System.out.println("Extractor::getFields: got "+l.size()+" fields for "+device+"/"+property);
               retval.putAll(l);
               
            }
        });
        
        return retval;
    
    }
    
    public static Map<String, String> getFields(Entity ent, Instant attime)
    {
    	System.out.println("Extractor::getFields: " + ent.getEntityKeyValues().get("device")+"/"+ent.getEntityKeyValues().get("property"));
    	System.out.println("Extractor::getFields: at time "+ attime);
    	SortedSet<EntityHistory> ehists = ent.getEntityHistory();
    	
    	for (EntityHistory ehist: ehists)
    	{
    		@NonNull
			TimeWindow valid = ehist.getValidity();
    		System.out.println("Extractor::getFields:        within time window "+ valid.toString()+" ? ");     
    		if (!valid.contains(attime)) 
    		 {
    			System.out.println("Extractor::getFields:        NO");
    			continue;
    		 }
    		System.out.println("Extractor::getFields:        YES");
    		return getFieldsFromSchema(ehist.getEntitySchema());
    		
    	}
    	
    	System.out.println("Extractor::getFields:    DID NOT FIND SCHEMA IN THE HISTORY");
    	return new TreeMap<String, String>();
    }
    
    /**
     * Returns map of fieldName - typeName 
     * @param entitySchema
     * @return
     */
    public static Map<String,String> getFieldsFromSchema(EntitySchema entitySchema)
    {
    	Map<String,String> retval = new TreeMap<String, String>();
    	ObjectMapper mapper = new ObjectMapper();
        try {
            JsonNode rootNode = mapper.readTree(entitySchema.getSchemaJson());
            JsonNode fieldsNode = rootNode.get("fields");
            if (fieldsNode.isArray()) {
            	
            	
                for (JsonNode field : fieldsNode) {
                	
                    String fieldName = field.get("name").asText();
                    JsonNode fo = field.get("type");
                    fo.size();
                    String typeName = "";
                    if (fo.isArray())
                    {
                    	fo = fo.get(0);
                    }
                    
                    if(fo.isTextual())
                    {
                       typeName = field.get("type").asText();
                    }
                    else 
                    {
                    	typeName = getArrayType(field.get("type"));
                    	
                    	if ( typeName.isEmpty() )
                    	{
                    	  System.out.println("Extractor::getFieldsFromSchema: Cannot determine type of field "+fieldName+" : "+ field);
                    	}
                      
                    }
                    
                    retval.put(fieldName, typeName);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    	return retval;
    }
    protected static String getArrayType(JsonNode jn)
    {
    	JsonNode jnn = jn.get("name");
    	if (jnn == null) return "";
    	
    	return  jnn.asText("");
    	
    }
    public Map<String,String> getFieldsAllTimes(String device, String property)
    {
    	
    	Map<String, Object> keyValues = ImmutableMap.of("device", device, "property", property);

        
        Entity entities = fEntityService.findOneWithHistory(
                Entities.suchThat().systemName().eq(fSystemSpecCMW.getName()).and().keyValues().eq(fSystemSpecCMW, keyValues),
                TimeUtils.getNanosFromString("2015-10-10 14:15:00.000000000"),
                TimeUtils.getNanosFromInstant(Instant.now())
        ).orElseThrow(() -> new IllegalArgumentException("Entity not found"));


        
        Map<String,String> retval = new TreeMap<String, String>();
        
        entities.getEntityHistory().forEach(entityHistory -> {
            System.out.println(entityHistory.getValidity());
            EntitySchema entitySchema = entityHistory.getEntitySchema();
            
            retval.putAll(getFieldsFromSchema(entitySchema));
        });
        
        return retval;
    
    }
    
    
    
    public Type getLastValuesType()
    {
    	return fLastValuesType;
    }

}
