package cern.accsoft.op.referencesaver.plotters;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JPanel;

import cern.accsoft.op.referencesaver.ReferenceSaverGuiApp;
import cern.accsoft.op.referencesaver.co.DataStore;
import cern.accsoft.op.referencesaver.gui.ReferenceViewPanel;
import cern.japc.core.FailSafeParameterValue;
import cern.japc.core.Parameter;
import cern.japc.core.ParameterException;
import cern.japc.core.factory.ParameterFactory;
import cern.japc.value.MapParameterValue;
import cern.japc.value.SimpleParameterValue;
import cern.jdve.scale.TimeStepsDefinition.TimeUnit;

abstract public class Plotter extends JPanel 
{
    private static final long serialVersionUID = 101001002301010L;

	ReferenceViewPanel  fParent =  null;

	/**
	 * This is full parameter name, with field
	 */
    protected String fFullParameterName;
    protected Parameter fParameter;
    protected String fLsaCycleName;
    
    protected Long fLastTimeStampMillis = null;
    
    
    public String getLsaCycleName()
    {
    	return fLsaCycleName;
    }
    public String getParameterName()
    {
    	return fFullParameterName;
    }
    
    public ReferenceViewPanel getReferenceViewPanel()
    {
    	return fParent;
    }
    public class Range
    {
    	public Double min = Double.MIN_VALUE;
    	public Double max = Double.MAX_VALUE;
    	
    	public Double getWidth()
    	{
    		return max - min;
    	}
    	
    }
    
    public Plotter(ReferenceViewPanel parent, String signal, String lsaCycleName) throws ParameterException 
    {
    	fParent = parent;
        fFullParameterName = signal;
        fLsaCycleName = lsaCycleName;
        System.out.println("Plotter::Plotter: creating parameter "+fFullParameterName+" ...");
        
        fParameter = ParameterFactory.newInstance().newParameter(fFullParameterName);
        
        System.out.println("Plotter::Plotter: creating parameter "+fFullParameterName+" ... Done");
        
        DataStore ds = ReferenceSaverGuiApp.getInstance().getDataStore();
        
        System.out.println("Plotter::Plotter: adding parameter to DataStore ...");
        ds.addParameter(fParameter.getDeviceName()+"/"+fParameter.getPropertyName(), this);
        System.out.println("Plotter::Plotter: adding parameter to DataStore ... Done");
        
        
    }
    
    public void finish()
    {
        DataStore ds = ReferenceSaverGuiApp.getInstance().getDataStore();
        ds.removeParameter(fParameter.getDeviceName()+"/"+fParameter.getPropertyName(), this);
        fParent = null;
    }

    public static String timeMiliToString(double value)
    {
        Date absTime = new Date((long) (TimeUnit.MILLISECOND.toMilliseconds(value)));
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        String label = dateFormat.format(absTime);
        return label;

    }
    

    
	public SimpleParameterValue getNotNullValue(FailSafeParameterValue val) throws NoSuchFieldException {
		
		
		if (val == null)
		{
			System.err.println("ScalarPlotter::getNotNullValue: "+fFullParameterName+" no value in received data event");
			
			throw new NoSuchFieldError(fFullParameterName+": passed value is null");
		}
		
		if (!val.isValue())
		{
			System.err.println("ScalarPlotter::getNotNullValue: "+fFullParameterName+" not a value: "+val);
			throw new NoSuchFieldError(fFullParameterName+": not a value: "+val);
		}
		
		SimpleParameterValue spv = null;
		if (val.getValue() instanceof SimpleParameterValue)
		{
		  spv = val.getValue().castAsSimple();
		}
		else if(val.getValue() instanceof MapParameterValue)
		{
		  MapParameterValue mpv = val.getValue().castAsMap();
		  int idx = fFullParameterName.indexOf("#");
		  String field = fFullParameterName.substring(idx+1); 
		  spv = mpv.get(field);
		}
		
		if (spv == null)
		{
			System.err.println("ScalarPlotter::getNotNullValue: "+fFullParameterName+" cannot convert to simple paramter");
			System.out.println(val.getValue());
			throw new NoSuchFieldError(fFullParameterName+": cannot convert to simple paramter ");
		}
		return spv;
	}
	abstract public void valueReceived(FailSafeParameterValue par, long cycleStamp);
	
	
}
