
package cern.accsoft.op.referencesaver.plotters;

import java.awt.Adjustable;
import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.time.Instant;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cern.accsoft.gui.frame.MessageManager;
import cern.accsoft.op.referencesaver.gui.ReferenceViewPanel;
import cern.accsoft.op.referencesaver.nxcals.Reference;
import cern.japc.core.FailSafeParameterValue;
import cern.japc.core.ParameterException;
import cern.japc.value.SimpleParameterValue;
//import cern.ctf.monitor.core.ChiSquareMonitor;
//import cern.ctf.monitor.core.DevicesInfoContainer;
//import cern.ctf.monitor.core.PropertyHandler;
//import cern.ctf.monitor.gui.MenuHandler;
//import cern.ctf.monitor.io.ReferenceStore;
import cern.jdve.Chart;
import cern.jdve.Scale;
import cern.jdve.Style;
import cern.jdve.data.DataSet;
import cern.jdve.data.DefaultDataSet;
import cern.jdve.data.DefaultDataSource;
import cern.jdve.data.ShiftingDataSet;
import cern.jdve.event.ChartInteractionEvent;
import cern.jdve.event.ChartInteractionListener;
import cern.jdve.event.DataSetEvent;
import cern.jdve.event.DataSetListener;
import cern.jdve.graphic.DataIndicator;
import cern.jdve.interactor.DataPickerInteractor;
import cern.jdve.interactor.DataRangePickerInteractor;
import cern.jdve.interactor.ZoomInteractor;
import cern.jdve.renderer.PolylineChartRenderer;
import cern.jdve.scale.TimeStepsDefinition;
import cern.jdve.utils.DataRange;
/**
 * Used to plot multiple chiSquare and update them while running.  
 * historic view
 * @version $ 1.1.0$ Date: 2010-04-21
 * @author <A HREF="http://consult.cern.ch/xwho/people/692826">Tobias Hakan Bjorn Persson</A>.
 */
public class ScalarPlotter extends Plotter {

    private static final Logger LOGGER = LoggerFactory.getLogger(ScalarPlotter.class);
    
    private static final long serialVersionUID = 1L;

    protected ReferenceViewPanel fParent; 
    
    //private static final int NUMBER_OF_ADDED_POINTS = 1;
    private Chart                fChart;
    private DefaultDataSource    fDataSource;
    private ShiftingDataSet      fDataSet;
    private PolylineChartRenderer fPolylineChartRenderer;
    
    private JScrollBar           sbVisibleRange;
    private ZoomInteractor       fZoomer;

    private int                  fNumberOfVisiblePoints;
    private UpdateTask           fUpdateTask;

    private boolean fIsOnline = true; 
    private double fLastValue = Double.NaN;
    
    private int fNumberOfPoints = 0;
    
    /**
     * It plot with single fake is created, and this fake point is removed at the arrival of first real data
     * and then it becomes not empty
     */
    private boolean fIsEmpty = true; 
    //private ChiSquareMonitor     chiMonitor;
    //private PropertyHandler      prop;
    //private DevicesInfoContainer deviceInfo;
    
    
    public ScalarPlotter(ReferenceViewPanel parent, String signal, String lsaCycleName, int numberOfPoints, double[] tin, double[] yin) throws ParameterException 
    {
      super(parent,signal,lsaCycleName);
      
      fParent = parent;
      fNumberOfPoints = numberOfPoints;
      
      double[] t = tin;
      double[] y = yin;
      if (tin == null || yin == null || tin.length < 1 || yin.length < 1)
      {
    	  t = new double[1];
    	  t[0] = Instant.now().toEpochMilli();
    	  y = new double[] {0.0};
    	  fIsEmpty = true;
      }
      else
      {
    	  fIsEmpty = false;
      }
    	  
      initComponents(t,y);
    }
    public void setEmpty()
    {
    	fIsEmpty = true;
    	fChart = null;
    }
    public ScalarPlotter(ReferenceViewPanel parent, String signal, String lsaCycleName, int numberOfPoints) throws ParameterException 
    {
      super(parent,signal,lsaCycleName);
      
      fParent = parent;
      fNumberOfPoints = numberOfPoints;
      
  	  fIsEmpty = true;
      
	  double[] t = new double[1];
	  t[0] = Instant.now().toEpochMilli();
	  double[] y = new double[] {0.0};

      initComponents(t,y);
    }
    //____________________________________________________________________
    
    public void addData(double[] tin, double[] yin)
    {
    	if (fDataSet == null )
    	{
    		return;
    	}
    	
    	if (! fIsEmpty)
    	{
    	  fDataSet.add(tin,yin);
    	  return;
    	}
    	
    	fDataSource.removeDataSet(0);
    	
        createDataSet( tin, yin);
        
    	
    	fIsEmpty = false;
    }
    //____________________________________________________________________
    
    public void finish()
    {
        super.finish();
        if (fUpdateTask == null)
        	return;
        
        fUpdateTask.cancel();
        fUpdateTask = null;
        fChart.removeAll();
        fDataSet.clear();
        for (int i=0; i<fDataSource.getDataSetsCount();i++)
        {
           fDataSource.removeDataSet(0);
        }
        fDataSet = null;
        fDataSource = null;
        this.remove(fChart);
        this.removeAll();
        
    }
    //____________________________________________________________________

    public void initComponents(double[] t, double[] y) {
        removeAll();
        setLayout(new BorderLayout());
        //chiMonitor = ChiSquareMonitor.getInstance();
        setPreferredSize(new Dimension(500, 200));

        add(createChart(t, y), BorderLayout.CENTER);
        add(createPanel(), BorderLayout.SOUTH);

        this.fDataSet.addDataSetListener(new DataSetListener() {
            @Override
            public void dataSetChanged(DataSetEvent evt) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
	                    	if (fDataSet != null)
                    	{
                          dataRangeChanged(fDataSet.getXRange());
                    	}
                    }
                });
            }
        });
    }
    //____________________________________________________________________
    //
    private Double[] getMinMax(double[] va)
    {
        Double []  minmax = new Double[2];
        minmax[0] = Double.MAX_VALUE;
        minmax[1] = Double.MIN_VALUE;
        if (va == null) return minmax; 
        if (va.length < 1) return minmax;
        
        for (double v: va)
         {
            if (v < minmax[0] ) minmax[0] = v;
            if (v > minmax[1] ) minmax[1] = v;
         }
        
        return minmax;
    }
    
    //____________________________________________________________________
    private ShiftingDataSet createDataSet(double[] t, double[] v) {
    	
        fDataSet = new ShiftingDataSet(fFullParameterName, fNumberOfPoints, true);
        fDataSet.add( t, v );
        fDataSource.addDataSet(0, fDataSet);
        
        fPolylineChartRenderer.setDataSource(fDataSource);
        this.fChart.addRenderer(fPolylineChartRenderer);
        
        
        
        return fDataSet;
        
    }
    
    //_____________________________________________________________
    private Chart createChart(double[] t, double[] v) {

        this.fChart = new Chart();


      //  this.dataSet = new ShiftingDataSet("RealTimeData", // name
      //          (int) prop.getNumberOfChiInView(), // buffer size
      //          true); // using X values
        
        
        fDataSource = new DefaultDataSource();


        //LOGGER.trace("The device " + devices[i]);
        
        LOGGER.info("createChart: Number of points is {}",fNumberOfPoints);

        Style style0 = new Style(new BasicStroke(2), Color.red, new Color(80, 128, 128, 128));
        fPolylineChartRenderer = new PolylineChartRenderer();
        fPolylineChartRenderer.setStyle(0, style0);
        style0.setStrokeColor(Color.red);

        createDataSet(t, v);
        fDataSource.addDataSet(0, fDataSet);
        


        //  polylineChartRenderer.setDataSet(shiftData);
        double lastT = 0.0; // last time stamp to put reference values at
        Range yrange = new Range();
        
        try
        { 
            LOGGER.trace("ChiSquareDeviceHistoryPlotter::createChart: Data size t {} v {}",t.length,v.length);
            fDataSet.add( t, v );
            
            lastT = t[t.length -1];
            
            Double[] minmax = getMinMax(v);
            yrange.min = minmax[0];
            yrange.max = minmax[1];

        }
        catch(Throwable e)
        {
            LOGGER.error("createChart: Looks like time array is null ",e);
        }

        
        //it adds to the range
        yrange = createReferenceLines( yrange, lastT);
        
        fChart.setDataSource(fDataSource);
        
        
        if ( yrange.max > yrange.min )
        {
           this.fChart.getYAxis().setVisibleMin(yrange.min-0.1*yrange.getWidth());
           this.fChart.getYAxis().setVisibleMax(yrange.max+0.1*yrange.getWidth());
        }
        else  
        {
           this.fChart.getYAxis().setRange(-10.0, +10.0);
        }
       
        
        
        
        /////////////
        
        this.fChart.getXScale().setStepsDefinition(new TimeStepsDefinition());

        
        fZoomer = new ZoomInteractor();
        fChart.addInteractor(fZoomer);
        
        this.fChart.getXAxis().setAutoRange(true);
        
        this.fChart.setLegendVisible(true);
        // Enable automatic scrolling of visible view- when new data comes in
        this.fChart.setShiftScroll(true);

        this.fChart.setDoubleBuffered(false);

        // We are interested only in values
        // chart.setXScaleVisible(false);

        // Start task adding new data

        fUpdateTask = new UpdateTask(this.fDataSource);
        Timer timer = new Timer();
        timer.schedule(fUpdateTask, 1500, 1500 );

        DataPickerInteractor dataPicker = new DataPickerInteractor();
        dataPicker.getPointCoordPane().getLabelRenderer().setBackground(new Color(204, 204, 255));

        dataPicker.addChartInteractionListener(new ChartInteractionListener() {
            @Override
            public void interactionPerformed(ChartInteractionEvent evt) {
            ///
            }
        });
        this.fChart.addInteractor(dataPicker);

        // add data range interactor
        DataRangePickerInteractor selectionInteractor = new DataRangePickerInteractor();

        selectionInteractor.addChartInteractionListener(new ChartInteractionListener() {

            @Override
            public void interactionPerformed(ChartInteractionEvent event) {
                //   DisplayPoint[] points = event.getDisplayPoints();
                event.getMouseEvent().getButton();

            }

        });
        this.fChart.addInteractor(selectionInteractor);
        boolean isLogView = false;
        if(isLogView) {
            fChart.getYAxis().setMin(0.01);
            Scale scale = new Scale();

            this.fChart.setYScale(scale);
            scale.setLogarithmic(10);

        }

        return this.fChart;
    }
    
    //____________________________________________________
    
    protected Range createReferenceLines( Range yrange, Double lastT )
    {
    	 /////////////////
        // Mark refernce values as straight h lines
        
    	
        List<Reference> refs = fParent.getReferences();
        
        for(int j = 0; j < refs.size(); j ++) 
        {

            Reference ref = refs.get(j); 
             
             
            SimpleParameterValue val = ref.getValue(fFullParameterName);
            if (val == null)
             {
            	 MessageManager.error("ScalarPlotter::createReferenceLines: Could not get reference value " +fFullParameterName+" from reference "+ ref.getName(), null, this);
            	 continue;
             }
             
            double refsignal = val.getDouble();
             

            if (refsignal > yrange.max) yrange.max =  refsignal;
            if (refsignal < yrange.min) yrange.min =  refsignal;
            
            DataIndicator di = new DataIndicator(DataIndicator.Y_VALUE,refsignal, ref.getName());
            di.getStyle().setStroke(new BasicStroke(2));
            di.setTextLocation(0.3);
            di.getLabelRenderer().setVerticalAlignment(SwingConstants.BOTTOM);
            
            di.getLabelRenderer().getFont().deriveFont(14f);
            
            //_____________________________________
            // Create a single point data set so zoom does not make the line disappear
            
            DefaultDataSet ds = new DefaultDataSet(ref.getName());
            ds.add(lastT, refsignal);
            this.fDataSource.addDataSet(ds);
            
            //_____________________________________
            // Set colors
            PolylineChartRenderer refRenderer = new PolylineChartRenderer();
            if (j ==0 )
            {
                di.getStyle().setStrokeColor(Color.blue);
                di.getLabelRenderer().setForeground(Color.blue);
                refRenderer.setStyle(j+1, new Style(Color.blue));
                
            }
            if (j == 1 )
            {
                di.getStyle().setStrokeColor(Color.green.darker());
                di.getLabelRenderer().setForeground(Color.green.darker());
                refRenderer.setStyle(j+1, new Style(Color.green.darker()));
            }
            
            fChart.addDecoration(di);
            
            refRenderer.setDataSource(fDataSource);
            fChart.addRenderer(refRenderer);
            
            
        }

        
        
        return yrange;
    }

/**
 *     
 * @return
 */
    //__________________________________________
    private JPanel createPanel() {
        JPanel pnlCommands = new JPanel(new BorderLayout());
        this.sbVisibleRange = new JScrollBar(Adjustable.HORIZONTAL);
        this.sbVisibleRange.setEnabled(false);
        this.sbVisibleRange.addAdjustmentListener(new AdjustmentListener() {
            @Override
            public void adjustmentValueChanged(AdjustmentEvent e) 
            {
                int value = e.getValue();
                // Max of data set range + value
                double max = fDataSet.getXRange().getMin() + value;
                
                
                double min = 0;
                if ( fDataSet.getDataCount() <  fNumberOfVisiblePoints)
                {
                    min = fDataSet.getX(0); 
                    
                }
                else
                {
                    min =fDataSet.getX(fDataSet.getDataCount() - fNumberOfVisiblePoints);
                }
                
                LOGGER.trace("ChiSquareDeviceHistoryPlotter::adjustmentValueChanged v="+value
                        +" min="+min+" max="+max);
                
                
                fChart.getXAxis().setVisibleRange(min, max);
                
            }
        });

        return pnlCommands;
    }
    /**
     * 
     * @param dataSetRange
     */
    void dataRangeChanged(DataRange dataSetRange) {
        if (!dataSetRange.isDefined()) {
            return;
        }
        if (dataSetRange.getLength() <= fNumberOfVisiblePoints) {
            return;
        } else if (!this.sbVisibleRange.isEnabled()) {
            this.sbVisibleRange.setEnabled(true);
        }

        // If the data set range is from 100 to 150 and the visible range length is 10
        // Then the min is set to 10, the max to 50 (150 - 100)
        // The value is set to max (100)
        int min = fNumberOfVisiblePoints;
        int max = (int) dataSetRange.getLength();
        this.sbVisibleRange.setValues(max, min, min, max);
    }


    private class UpdateTask extends TimerTask {
        private final DefaultDataSource dataSource;

        UpdateTask(DefaultDataSource dataSource) {
            this.dataSource = dataSource;
        }

        @Override
        public void run() 
        {
            if(!fIsOnline) 
            {
            	return;
            }
            
            if(fZoomer.isInOperation()) 
            {
            	return;
            }

            if (Double.isNaN(fLastValue))
            {
            	return;
            }
            // chart.getXAxis().setMin(0.01);
            //LOGGER.trace("ChiSquareDeviceHistoryPlotter::UpdateTask::run Updating multiploter");
            //long time = System.currentTimeMillis();
            if (fIsEmpty)
            {
            	fDataSource.removeDataSet(0);
            	double[] ta = new double[1];
            	double[] va = new double[1];
            	ta[0] = fLastTimeStampMillis;
            	va[0] = fLastValue;
                createDataSet( ta, va);
                
            	
            	fIsEmpty = false;

            	fLastValue = Double.NaN;
            	return;
            }
            this.dataSource.getDataSet(0).add(fLastTimeStampMillis, fLastValue);

            // adjust reference markings to the current time 
            for (int i=1; i< dataSource.getDataSetsCount(); i++)
            {
            	DataSet ds = dataSource.getDataSet(i);
            	ds.set(0, fLastTimeStampMillis, ds.getY(0));
            }
            
            fLastValue = Double.NaN;


        }
    }
    /**
     * Cancel the automatic update.
     */
    public void cancelUpdate() {
        fUpdateTask.cancel();
    }
    /**
     * 
     * @return chart Returns the chart for the object.
     */
    public Chart getChart() {
        return fChart;

    }

	@Override
	public void valueReceived(FailSafeParameterValue val, long cycleStampNanos) 
	{
		
		SimpleParameterValue spv ;
		try 
		{
			spv = getNotNullValue(val);
			fLastValue = spv.getDouble();
			fLastTimeStampMillis = cycleStampNanos/1000000;
			
		} 
		catch (Throwable e) 
		{
			// it simply could ne wrong user
			//System.err.println("No good data in data event "+ e.getMessage());
			e.printStackTrace();
			System.out.println("ScalarPlotter::dataOccurred: No good data for us in this shot "+e.getMessage());
			return;
		}
		

		
		System.out.println("ScalarPlotter::valueReceived new value of "+fFullParameterName+" = "+ fLastValue);
		//
	}
    
}

