/*
 * $Id: RealTimeDemo.java,v 1.5 2009-03-06 09:29:54 gkruk Exp $
 * 
 * $Date: 2009-03-06 09:29:54 $ $Revision: 1.5 $ $Author: gkruk $
 * 
 * Copyright CERN, All Rights Reserved.
 */
package cern.accsoft.op.referencesaver.plotters;


import java.awt.Adjustable;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.SwingUtilities;

import org.apache.hadoop.hive.ql.parse.HiveParser_IdentifiersParser.sysFuncNames_return;

import cern.accsoft.op.referencesaver.ReferenceSaverGuiApp;
import cern.accsoft.op.referencesaver.gui.ReferenceViewPanel;
import cern.japc.context.Context;
import cern.japc.context.CtxDataEvent;
import cern.japc.core.FailSafeParameterValue;
import cern.japc.core.ParameterException;
import cern.japc.value.SimpleParameterValue;
import cern.jdve.Chart;
import cern.jdve.Scale;
import cern.jdve.data.AbstractDataSet;
import cern.jdve.data.ShiftingDataSet;
import cern.jdve.event.DataSetEvent;
import cern.jdve.event.DataSetListener;
import cern.jdve.interactor.ZoomInteractor;
import cern.jdve.scale.TimeStepsDefinition;
import cern.jdve.utils.DataRange;

/**
 * This class is based on the demo for ShiftingDataSet written by gkruk
 *  $Id: RealTimeDemo.java,v 1.5 2009-03-06 09:29:54 gkruk Exp $
 * @version $ 1.1.0$ Date: 2010-04-21
 * @author <A HREF="http://consult.cern.ch/xwho/people/692826">Tobias Hakan Bjorn Persson</A>.
 * 
 * 
 */
public class SomePlotter extends Plotter  {

    private static final long serialVersionUID = 1L;

    private int fNumberOfAddedPoints = 0;
    
    
    private Chart chart;
    private ShiftingDataSet dataSet;
    private JScrollBar sbVisibleRange;
    int              fNumberOfVisiblePoints;
    //  private boolean pause = false;
    private UpdateTask updateTask;
    private Timer fTimer;
    private boolean pause = false;
    private int fNumberOfPoints = 0;
    private double fLastValue = Double.NaN;
    private boolean fIsOnline = true; 
    
    
    public SomePlotter(ReferenceViewPanel parent, String signal, String selectorName, int numberOfPoints) throws ParameterException {
        
    	super(parent,signal,selectorName);
    	

    	fNumberOfPoints = numberOfPoints;
        initComponents();
    }
    
    
    public void finish()
    {
    	
    	super.finish();
        
        updateTask.cancel();
        fTimer.cancel();
        fTimer.purge();
        
        updateTask = null;
        fTimer = null;
        
    }
    
    private void initComponents() 
    {
        setLayout(new BorderLayout());
        
        setPreferredSize(new Dimension(500, 200));
        add(createChart(), BorderLayout.CENTER);
        add(createPanel(), BorderLayout.SOUTH);

        this.dataSet.addDataSetListener(new DataSetListener() {
            @Override
            public void dataSetChanged(DataSetEvent evt) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        dataRangeChanged(SomePlotter.this.dataSet.getXRange());
                    }
                });
            }
        });
    }
    protected double[] getHorValues()
    {
    	double[] retval = new double[] {1.0};  
    	return retval;
    	
    }
    
    protected double[] getVerValues()
    {
    	//devices is fSignal 
    	double[] retval = new double[] {1.0};  
    	return retval;
    	
    }

    private Chart createChart() {
        this.chart = new Chart();

        this.dataSet = new ShiftingDataSet("RealTimeData", // name
                fNumberOfPoints, // buffer size
                true); // using X values

        boolean isLogView = false;
        if(isLogView) {
            chart.getYAxis().setMin(0.01);
            Scale scale = new Scale();
            this.chart.setYScale(scale);
            scale.setLogarithmic(10);

        }
        
        this.dataSet.add(getHorValues(), getVerValues());
        //     this.dataSet = chiMonitor.getDataSetDevice(fSignal);
        this.chart.setDataSet(this.dataSet);


        this.chart.getXScale().setStepsDefinition(new TimeStepsDefinition());
        // We know that data is always from this range - let's fix it
        //    this.chart.getYAxis().setRange(0.0, 1.5);
        
        
        //    this.chart.getYAxis().setRange(Min(fSignal), Max(fSignal));

        // The buffer size is 1000 - but we want to see only last 100 points
        // It is a bit tricky - we must specify the visible range but visible range must be
        // in bounds of the range thus first we specify range, then visible range and
        // at the end we enable back auto update
        chart.addInteractor(new ZoomInteractor());

        fNumberOfVisiblePoints = 2000;
        
        this.chart.getXAxis().setAutoRange(true);
        
        this.chart.setLegendVisible(true);
        // Enable automatic scrolling of visible view- when new data comes in
        this.chart.setShiftScroll(true);

        this.chart.setDoubleBuffered(false);

        // We are interested only in values
        // chart.setXScaleVisible(false);

        // Start task adding new data
        updateTask = new UpdateTask(this.dataSet);
        fTimer = new Timer();
        fTimer.schedule(updateTask, 500, // Start update in 2sec.
                1500 );

        return this.chart;

    }
    private JPanel createPanel() {
        JPanel pnlCommands = new JPanel(new BorderLayout());
        this.sbVisibleRange = new JScrollBar(Adjustable.HORIZONTAL);
        this.sbVisibleRange.setEnabled(false);
        this.sbVisibleRange.addAdjustmentListener(new AdjustmentListener() 
        {
            @Override
            public void adjustmentValueChanged(AdjustmentEvent e) {
                int value = e.getValue();
                // Max of data set range + value
                double max = dataSet.getXRange().getMin() + value;
                
                double min = 0;
                if ( dataSet.getDataCount() <  fNumberOfVisiblePoints)
                {
                    min =dataSet.getX(0); 
                }
                else
                {
                    min =dataSet.getX(dataSet.getDataCount() - fNumberOfVisiblePoints);
                }
                
                SomePlotter.this.chart.getXAxis().setVisibleRange(min, max);
                System.out.println("ChiSquareMonitorPloter::adjustmentValueChanged v="+value
                        +" min="+min+" max="+max);

            }
        });
        pnlCommands.add(this.sbVisibleRange);
        final int value = 500;
        final JButton btnPause = new JButton("START");
        
        btnPause.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                if ("STOP".equals(evt.getActionCommand())) {
                    btnPause.setText("Sart online");
                    SomePlotter.this.pause = true;
                } else {
                    btnPause.setText("STOP");
                    SomePlotter.this.pause = false;
                }
                double max = SomePlotter.this.dataSet.getXRange().getMin() + value;
                double min = max - fNumberOfVisiblePoints;
                SomePlotter.this.chart.getXAxis().setVisibleRange(min, max);
            }
        });
        
        pnlCommands.add(btnPause, BorderLayout.SOUTH);
        
        return pnlCommands;
    }

    void dataRangeChanged(DataRange dataSetRange) {
        if (!dataSetRange.isDefined()) {
            return;
        }
        if (dataSetRange.getLength() <= fNumberOfVisiblePoints) {
            return;
        } else if (!this.sbVisibleRange.isEnabled()) {
            this.sbVisibleRange.setEnabled(true);
        }

        // If the data set range is from 100 to 200 and the visible range length is 10
        // Then the min is set to 10, the max to 100 (200 - 100)
        // The value is set to max (100)
        int min = fNumberOfVisiblePoints;
        int max = (int) dataSetRange.getLength();
        this.sbVisibleRange.setValues(max, min, min, max);
    }



    private class UpdateTask extends TimerTask {
        private final AbstractDataSet taskdataSet;
        //private final Random rnd = new Random(System.currentTimeMillis());

        UpdateTask(AbstractDataSet dataSet) {
            this.taskdataSet = dataSet;
        }


        @Override
        public void run() {
            if (SomePlotter.this.pause) {
                return;
            }
            if(fIsOnline) 
            {
                for (int i = 0; i < fNumberOfAddedPoints; i++) {
                    // The data set is not using X values - thus X coordinate is ignored
                    this.taskdataSet.add(System.currentTimeMillis(),fLastValue);
                }
                
                fNumberOfAddedPoints = 0;
            }
        }}

    public void cancelUpdate() {
        updateTask.cancel();
    }

	@Override
	public void valueReceived(FailSafeParameterValue val, long cycleStamp) 
	{

		SimpleParameterValue spv ;
		try 
		{
			spv = getNotNullValue(val);
		} 
		catch (Throwable e) 
		{
			// it simply could ne wrong user
			//System.err.println("No good data in data event "+ e.getMessage());
			System.out.println("ScalarPlotter::dataOccurred: No good data for us in this shot ");
			return;
		}

		fLastValue = spv.getDouble();

		fNumberOfAddedPoints = 1;
		
		System.out.println("ScalarPlotter::dataOccurred new value of "+fFullParameterName+" = "+ fLastValue);
		//SwingUtilities.invokeLater(() -> )
	}

    


}