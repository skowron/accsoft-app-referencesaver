package cern.accsoft.op.referencesaver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Config {

	protected static final Logger LOGGER = LoggerFactory.getLogger(Config.class);
	public static boolean fDevConfig = false;
	
	public static void setDev(boolean flag)
	{
		fDevConfig = flag;
	}
	
	public enum AccelConfig
	{
		LN4("PSB.USER.ALL","PSB:NXCALS_FUNDAMENTAL","LN4.REFERENCE/ReferenceTag#value","PSB.TIMING.INFO/AcquisitionReady");  //"XTIM.BX.SCY-CT/Acquisition"
		
		public String fPlsAll;
		public String fFundamentalVariable;
		public String fReferenceNameParam;
		public String fTimingInfoParam;
		
		AccelConfig (String plsAll, String fundVarName, String refNameParam, String tgmDev)
		{
			fPlsAll = plsAll;
			fFundamentalVariable = fundVarName;
			fReferenceNameParam = refNameParam;
			fTimingInfoParam = tgmDev;
		}
	};
	
	static private AccelConfig fAccelConfig = AccelConfig.LN4;
	
    static public void setAccelerator(String name)
    {
       AccelConfig newconf = AccelConfig.valueOf(name);
       if (newconf == null)
       {
    	   LOGGER.error("Canot find configuration for {}. Did not change it, it is still {}",name, fAccelConfig.toString());
    	   return;
       }
    		   
       fAccelConfig = newconf;       
    }
	
    static public void setAccelerator(AccelConfig accconf)
    {
       fAccelConfig = accconf; 
    }
    
    static public String getReferenceNameParam()
    {
    	return fAccelConfig.fReferenceNameParam;
    }
    
    static public String getTimingInfoParameter()
    {
        return fAccelConfig.fTimingInfoParam;
    }
    static public String getPlsAll()
    {
    	return fAccelConfig.fPlsAll;
    }
    
    static public String getFundamentalVariable()
    {
    	return fAccelConfig.fFundamentalVariable;
    }
    
    static public String getAcceleratorName()
    {
    	return fAccelConfig.name();
    }
    static public boolean isDev()
    {
    	return fDevConfig;
    }
  
}
