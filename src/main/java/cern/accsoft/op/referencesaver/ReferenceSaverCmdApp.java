/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.accsoft.op.referencesaver;

import java.time.Instant;

import org.apache.spark.sql.SparkSession;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Import;

import cern.accsoft.ccs.ccda.client.core.CcdaClient;
import cern.accsoft.ccs.ccda.client.core.Environment;
import cern.accsoft.ccs.ccda.client.model.accelerator.Accelerator;
import cern.accsoft.commons.ccs.CcdaUtils;
import cern.accsoft.op.referencesaver.nxcals.Tagging;
import cern.nxcals.api.config.SparkContext;
import cern.nxcals.api.custom.domain.Tag;
import cern.nxcals.common.Constants;
import cern.rbac.client.authentication.AuthenticationClient;
import cern.rbac.client.authentication.AuthenticationException;
import cern.rbac.common.RbaToken;
import cern.rbac.util.holder.ClientTierTokenHolder;

@Import(SparkContext.class)
@SpringBootApplication
public class ReferenceSaverCmdApp {
    
    static Logger log = LoggerFactory.getLogger(ReferenceSaverCmdApp.class);
    
    
    //________________________________________________________
    
    static {
        
        System.setProperty("service.url","https://cs-ccr-nxcals6.cern.ch:19093,https://cs-ccr-nxcals7.cern.ch:19093,https://cs-ccr-nxcals8.cern.ch:19093");
        System.setProperty("NXCALS_RBAC_AUTH", "true");
        System.setProperty(CcdaClient.CCDA_ENV, Environment.PRO.name());
        System.setProperty(Constants.KERBEROS_AUTH_DISABLE, "true");
    }

    //________________________________________________________
    

    @SuppressWarnings("unused") // it is used by Spring
    private static void login() {

        try {
            AuthenticationClient authenticationClient = AuthenticationClient.create();
            //Don't commit your password here!!!
            RbaToken token = authenticationClient.loginKerberos();
            
            ClientTierTokenHolder.setRbaToken(token);
        } catch (AuthenticationException e) {
            throw new IllegalArgumentException("Cannot login", e);
        }

    }
    //________________________________________________________
    
    @NotNull
    private static SparkSession getSparkSession(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(ReferenceSaverCmdApp.class, args);
        SparkSession sparkSession = context.getBean(SparkSession.class);
        
        log.info("Got spark session {}", sparkSession);
        return sparkSession;
    }


    //________________________________________________________

    public static void main(String[] args) {
        
        login();

        String thisAcceleratorName = "LN4";
        Accelerator thisAccelerator = CcdaUtils.getAccelerator(thisAcceleratorName);
        if (thisAccelerator == null)
        {
            log.error("Cannot find accelerator " + thisAcceleratorName);
            return;
        }
        
        //Set<String> classes = CcdaUtils.getDeviceClassNamesByAccelerator(thisAccelerator.getName());
        //log.info("main: found "+classes.size() + " classes");
        
        
        SparkSession sparkSession = getSparkSession(args);
        
        Tagging tagging = new Tagging(sparkSession, thisAccelerator);
        
        String command = "extract";
        String tagName = "TEST_TAG_1715007781371"; //"TEST_TAG_1715000487590"; //1713977882064
        
        Tag tag = null;
        if (command.contains("create"))
        {
        //Tag creation
        long ts = tagging.findSomeTimestamp();
        String tagname = "TEST_TAG_" + Instant.now().toEpochMilli();
        tag = tagging.createTag(tagname, ts);
        
        if (tag == null)
        {
            log.error("ReferenceSaverCmdApp: tagging failed");
            sparkSession.close();
            System.exit(0);
            return;
        }
        }
        else if (command.contains("update"))
        {
        //Updating tag metadata
        //Tagging.findAndUpdateTag(tagService,tag.getName(), "New description");
        }
        else if (command.contains("extract"))
        {
        //Getting some data from a Tag info
            
        tag = tagging.getTag(tagName);
        
        System.out.println("Tag: "+ tag);
        //tag.getProperties();
        
        tagging.getDataFromTag( tag);
        
        }
        else if (command.contains("delete"))
        {
           //Deleting a tag
           tagging.deleteTag(tag);
        }
        
        sparkSession.close();
        System.exit(0);

    }
}
